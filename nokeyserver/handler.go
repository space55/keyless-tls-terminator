package nokeyserver

import (
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
	"gitlab.com/space55/keyless-tls-terminator/server"
)

func (s *NoKeyServer) handleOp(callback server.ResponseCallback, op *proto.Op) error {
	kttlog.Logger.Debug().Uint32("op_id", uint32(op.GetID())).Msg("NoKeyServer.handleOp: Handling op")
	switch op.GetType() {
	case proto.OpType_DEBUG:
		kttlog.Logger.Info().Uint32("op_id", uint32(op.GetID())).Str("debug_msg", string(op.GetContent())).Msg("NoKeyServer.handleOp: Received debug message from remote")
	case proto.OpType_PROOFREQ:
		kttlog.Logger.Debug().Uint32("op_id", uint32(op.GetID())).Msg("NoKeyServer.handleOp: Received proof request from remote")
		proofResp, err := s.handleProofReq(op.GetID(), op.GetContent())
		if err != nil {
			return err
		}
		kttlog.Logger.Trace().Uint32("op_id", uint32(op.GetID())).Msg("NoKeyServer.handleOp: Sending proof response to remote")
		err = callback(proofResp)
		if err != nil {
			return err
		}
	default:
		kttlog.Logger.Warn().Uint32("op_id", uint32(op.GetID())).Uint8("op_type", uint8(op.GetType())).Msg("NoKeyServer.handleOp: Received unknown op type")
	}

	return nil
}

func (s *NoKeyServer) handler(sendCallback server.ResponseCallback, msg *proto.Message) error {
	kttlog.Logger.Info().Uint32("id", uint32(msg.GetID())).Msg("NoKeyServer.handler: Handling message")
	ops := msg.GetOps()
	for i := range ops {
		err := s.handleOp(sendCallback, &ops[i])
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("NoKeyServer.handler: Error handling op")
		}
	}

	return nil
}
