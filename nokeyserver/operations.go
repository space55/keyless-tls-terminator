package nokeyserver

import (
	"crypto"
	"errors"

	"gitlab.com/space55/keyless-tls-terminator/keystore"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func (s *NoKeyServer) handleProofReq(opID proto.OpID, data []byte) (*proto.Op, error) {
	op, err := proto.Unmarshal_ProofReq(data)
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Trace().Str("domain", op.Domain).Str("hash_func", op.HashFunc.String()).Msg("NoKeyServer.handleProofReq: Received proof request")

	proof, err := s.signWithLoad(op.Domain, op.HashFunc, op.SigType, op.Challenge)
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Trace().Str("domain", op.Domain).Int("len", len(proof)).Msg("NoKeyServer.handleProofReq: Successfully signed challenge")

	rep := &proto.Op_ProofRep{
		Proof:        proof,
		ReplyingToID: opID,
	}

	out, err := rep.Marshal()
	if err != nil {
		return nil, err
	}

	respOp, err := proto.NewOp(proto.OpType_PROOFREP, out)
	if err != nil {
		return nil, err
	}

	return respOp, nil
}

func (s *NoKeyServer) signWithLoad(domain string, hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte) ([]byte, error) {
	proof, err := s.Keystore.Sign(domain, hashFunc, sigType, challenge)
	if err != nil {
		if errors.Is(err, &keystore.KeyNotFoundError{}) && s.DomainToKey != nil {
			key := s.DomainToKey(domain)
			if key != nil {
				s.Keystore.AddKey(key)
				return s.Keystore.Sign(domain, hashFunc, sigType, challenge)
			}
		}

		return nil, err
	}

	return proof, err
}
