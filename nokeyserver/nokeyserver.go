package nokeyserver

import (
	"crypto/x509"

	"gitlab.com/space55/keyless-tls-terminator/keystore"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/server"
)

// NewNoKeyServerFromParams creates a new NoKeyServer from the given parameters. certpath and keypath should be a TLS
// certificate used to handle connections from any NoKeyClient, and capath should be a CA certificate used to verify
// both its own certificate and the certificate of the remote NoKeyClient. It will return a new *NoKeyServer,
// and any error encountered loading the key pair, creating the cert pool, or creating the internal server.Server
// struct.
func NewNoKeyServerFromParams(lnetwork, laddr, certpath, keypath string, capath []string) (*NoKeyServer, error) {
	kttlog.Logger.Debug().Msg("nokeyserver.NewNoKeyServerFromParams: Creating new NoKeyServer from parameters")
	cert, err := netcommon.LoadKeyPair(certpath, keypath)
	if err != nil {
		return nil, err
	}

	rootCAs, err := netcommon.NewCertPool(capath)
	if err != nil {
		return nil, err
	}

	s := &NoKeyServer{
		localAddr:    laddr,
		localNetwork: lnetwork,
		cert:         cert,
		rootCAs:      rootCAs,
		Keystore:     keystore.NewKeyStore("default"),
	}

	srv := server.NewServer(lnetwork, laddr, s.handler, *cert, *rootCAs)

	s.server = srv

	return s, nil
}

// NewNoKeyServer creates a new *NoKeyServer from a config file located at the given path. It internally calls
// NewNoKeyServerFromParams, and applies any additional necessary configuration from the config file. It will return
// the new *NoKeyServer, and any error encountered loading the config file, or from the internal call to NewNoKeyServerFromParams.
func NewNoKeyServer(configPath string) (*NoKeyServer, error) {
	kttlog.Logger.Debug().Str("path", configPath).Msg("nokeyserver.NewNoKeyServer: Creating new NoKeyServer from config file")
	conf, err := readConfig(configPath)
	if err != nil {
		return nil, err
	}

	s, err := NewNoKeyServerFromParams(conf.LocalNetwork, conf.LocalAddr, conf.CertPath, conf.KeyPath, conf.CAPath)
	if err != nil {
		return nil, err
	}

	err = applyConfig(conf, s)
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Trace().Msg("nokeyserver.NewNoKeyServer: NoKeyServer created")

	return s, nil
}

func (s *NoKeyServer) keyRetriever(domain string) (*keystore.Key, error) {
	kttlog.Logger.Info().Str("domain", domain).Msg("NoKeyServer.keyRetriever: Retrieving key for domain")

	if s.Datastore == nil {
		return nil, &NoDatastoreError{}
	}

	cert, err := s.Datastore.GetCertificate(domain)
	if err != nil {
		return nil, err
	}

	key, err := s.Datastore.GetPrivateKey(domain)
	if err != nil {
		return nil, err
	}

	k, err := keystore.NewKeyFromBytes(cert, key)
	if err != nil {
		return nil, err
	}

	return k, nil
}

// Run starts the NoKeyServer. This function is different from Start, as it will block
// until the server is stopped. Use this if your main goroutine is supposed to be handling the server loop.
func (s *NoKeyServer) Run() {
	s.server.StartBlocking()
}

// Start starts the NoKeyServer, and then returns once the server is ready to accept connections.
// This function is different from Run, as it will not block. Use this if your main goroutine is not supposed to be
// handling the main server loop.
func (s *NoKeyServer) Start() {
	s.server.StartBlockingUntilReady()

	kttlog.Logger.Debug().Msg("NoKeyServer.Start: Server started")
}

// Close closes the NoKeyServer.
func (s *NoKeyServer) Close() {
	s.server.Close()
}

// LoadKey will load a *keystore.Key into the NKS, allowing operations to be performed by that key on its domain.
func (s *NoKeyServer) LoadKey(key *keystore.Key) {
	s.Keystore.AddKey(key)
}

// LoadKeyFromFile will read a certificate and private key from the provided paths, create a *keystore.Key, and store
// it in the NKS. It internally calls AddKey on its keystore.Keystore struct. It will return
// any error encountered reading or parsing the files.
func (s *NoKeyServer) LoadKeyFromFile(crtpath string, keypath string) error {
	key, err := keystore.NewKeyFromFile(crtpath, keypath)
	if err != nil {
		return err
	}

	s.Keystore.AddKey(key)
	return nil
}

// LoadKeysFromDir loads all of the keys from the given path into the NKS. It will return any error encountered
// during the loading of the keys.
func (s *NoKeyServer) LoadKeysFromDir(path string) error {
	kttlog.Logger.Info().Str("path", path).Msg("NoKeyServer.LoadKeysFromDir: Loading keys from directory")
	acceptor := func(k *x509.Certificate) bool {
		return true
	}
	return s.Keystore.ReadLocalKeys(path, acceptor)
}
