// nokeyserver is the package that provides the NoKeyServer type. It is the server that handles operations requested
// by clients, and stores the keys and certificates that are used to sign challenges.
package nokeyserver

import (
	"crypto/tls"
	"crypto/x509"

	"gitlab.com/space55/keyless-tls-terminator/datastore"
	"gitlab.com/space55/keyless-tls-terminator/keystore"
	"gitlab.com/space55/keyless-tls-terminator/proto"
	"gitlab.com/space55/keyless-tls-terminator/server"
)

// DomainToKeyFunc is a function that will accept a given domain, and return a *keystore.Key struct
// that is capable of performing signature operations for the given domain.
type DomainToKeyFunc func(domain string) *keystore.Key

// NoKeyServer is the primary struct that handles signatures. It is responsible for performing signature operations,
// loading certificates and keys, and managing the TLS connections to the remote clients. Note that it supports
// external datastores, provided by the datastore package. These datastores are used to store and retrieve keys,
// as for large deployments, it doesn't make sense to load every single key into memory at start. Not to mention,
// it reduces the potential security implications of a compromised server (reducing the number of private keys
// stored in memory)
type NoKeyServer struct {
	localAddr    string
	localNetwork string
	cert         *tls.Certificate
	server       *server.Server
	rootCAs      *x509.CertPool
	messageBuf   *proto.Message
	Keystore     *keystore.KeyStore
	DomainToKey  DomainToKeyFunc
	Datastore    datastore.Datastore
}

// NKSConfig is the struct that the config file is marshalled into.
type NKSConfig struct {
	LocalAddr            string           `toml:"bind_addr"`
	LocalNetwork         string           `toml:"bind_network"`
	CertPath             string           `toml:"server_cert_path"`
	KeyPath              string           `toml:"server_key_path"`
	CAPath               []string         `toml:"ca_paths"`
	RemoteKeysType       string           `toml:"remote_keys_type"`
	MemcachedConfig      *MemcachedConfig `toml:"memcached"`
	RedisConfig          *RedisConfig     `toml:"redis"`
	DirectoryConfig      *DirectoryConfig `toml:"directory"`
	VerifySentSignatures bool             `toml:"verify_sent_signatures"`
	LogFile              string           `toml:"log_file"`
	EnableSyslog         bool             `toml:"enable_syslog"`
	SyslogNetwork        string           `toml:"syslog_network"`
	SyslogAddr           string           `toml:"syslog_addr"`
}

type MemcachedConfig struct {
	Addresses []string `toml:"addresses"`
	Network   string   `toml:"network"`
}

type RedisConfig struct {
	Address  string `toml:"address"`
	Network  string `toml:"network"`
	Password string `toml:"password"`
	DB       int    `toml:"db"`
}

type DirectoryConfig struct {
	Path string `toml:"path"`
}

// NoDatastoreError is the error returned when the datastore is not configured.
type NoDatastoreError struct{}

// Error returns the error message for a NoDatastoreError
func (e *NoDatastoreError) Error() string {
	return "No datastore configured"
}
