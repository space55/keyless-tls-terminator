package nokeyserver

import (
	"github.com/BurntSushi/toml"

	"gitlab.com/space55/keyless-tls-terminator/datastore"
	memcached_datastore "gitlab.com/space55/keyless-tls-terminator/datastore/memcached"
	redis_datastore "gitlab.com/space55/keyless-tls-terminator/datastore/redis"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func applyConfig(conf *NKSConfig, s *NoKeyServer) error {
	kttlog.Logger.Debug().Msg("nokeyserver.applyConfig: Applying config")
	kttlog.Logger.Trace().Str("remote_keys_type", conf.RemoteKeysType).Msg("nokeyserver.applyConfig: Got remote keys type")
	var ds datastore.Datastore
	var err error
	switch conf.RemoteKeysType {
	case "directory":
		err := s.LoadKeysFromDir(conf.DirectoryConfig.Path)
		if err != nil {
			return err
		}
	case "redis":
		ds, err = redis_datastore.NewRedisDatastore(conf.RedisConfig.Address, conf.RedisConfig.Network, conf.RedisConfig.Password, conf.RedisConfig.DB)
	case "memcached":
		ds, err = memcached_datastore.NewMemcachedDatastore(conf.MemcachedConfig.Addresses, conf.MemcachedConfig.Network)
	default:
		return &NoDatastoreError{}
	}

	if err != nil {
		return err
	}

	if conf.VerifySentSignatures {
		s.Keystore.VerifySentSignatures = true
	}

	if conf.LogFile != "" {
		err = kttlog.EnableFileLog(conf.LogFile)
		if err != nil {
			return err
		}
	}

	if conf.EnableSyslog {
		if conf.SyslogNetwork == "" {
			err = kttlog.EnableLocalSyslog()
			if err != nil {
				return err
			}
		} else {
			err = kttlog.EnableNetworkSyslog(conf.SyslogNetwork, conf.SyslogAddr)
			if err != nil {
				return err
			}
		}
	}

	s.Datastore = ds

	s.Keystore.KeyRetriever = s.keyRetriever

	kttlog.Logger.Trace().Msg("nokeyserver.runConfigDefaults: Finished running config defaults")

	return nil
}

func getDefaults() *NKSConfig {
	conf := &NKSConfig{
		LocalNetwork:    "tcp",
		DirectoryConfig: &DirectoryConfig{},
		RedisConfig: &RedisConfig{
			Network: "tcp",
		},
		MemcachedConfig: &MemcachedConfig{
			Network: "tcp",
		},
	}

	return conf
}

func readConfig(path string) (*NKSConfig, error) {
	conf := getDefaults()

	_, err := toml.DecodeFile(path, conf)

	kttlog.Logger.Trace().Msg("nokeyserver.readConfig: Decoded config")

	return conf, err
}
