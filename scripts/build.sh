#!/bin/bash

set -e -u -x
set -o pipefail

if [[ -d "build" ]]; then
    rm -rf build
fi

function get_url() {
    echo "https://ktt-assets.space55.cloud/ktt/${1}/${2}"
}

function upload_files() {
    pushd build
    echo "${REFTAG_LINE}" >"${MANIFEST_FILENAME}"
    echo "" >>"${MANIFEST_FILENAME}"

    for f in *; do
        SIZE=$(du -h --apparent-size "$f" | awk '{print $1}')
        echo "${SIZE} ${f} $(get_url "$1" "$f")" >>"${MANIFEST_FILENAME}"
    done

    aws s3 cp --recursive ./ "s3://ktt-assets.space55.cloud/ktt/${1}/"
    popd
}

function upload_release() {
    pushd build
    for f in *; do
        URL=$(get_url "$1" "$f")
        printf '[%s](%s)\n' "$URL" "$URL" >>"../${RELEASE_FILENAME}"
    done
    popd
}

# Setup
GIT_REF=$(git rev-parse HEAD)
GIT_TAG=$(git name-rev --tags --name-only "$(git rev-parse HEAD)")
MANIFEST_FILENAME="manifest.txt"
RELEASE_FILENAME="release.txt"

REFTAG_LINE="ref: ${GIT_REF}"
if [[ "${GIT_TAG}" != "undefined" ]]; then
    REFTAG_LINE=$(printf '%s\ntag: %s\n' "${REFTAG_LINE}" "${GIT_TAG}")
else
    GIT_TAG="development"
fi

# Build
go build -ldflags "-s -w -X main.version=${GIT_TAG}"

mkdir -p build

# Package & sign
upx -9 -o build/ktt keyless-tls-terminator
sha256sum build/ktt >build/ktt.sha256.txt
sha1sum build/ktt >build/ktt.sha1.txt

# Upload files, generate release.txt
get_url "$GIT_REF" "$MANIFEST_FILENAME" >${RELEASE_FILENAME}

upload_files "${GIT_REF}"
if [[ "${GIT_TAG}" != "development" ]]; then
    upload_files "${GIT_TAG}"
    get_url "$GIT_TAG" "$MANIFEST_FILENAME" >${RELEASE_FILENAME}
    upload_release "$GIT_TAG"
else
    upload_release "$GIT_REF"
fi

printf '\n---\n' >>${RELEASE_FILENAME}
cat "build/${MANIFEST_FILENAME}" >>${RELEASE_FILENAME}
