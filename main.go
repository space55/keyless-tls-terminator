package main

import (
	"os"

	"github.com/alecthomas/kong"
	"gitlab.com/space55/keyless-tls-terminator/keyloader"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/nokeyserver"
	"gitlab.com/space55/keyless-tls-terminator/proxy"
)

var version = "development"

type CLIContext struct {
}

type ProxyCmd struct {
	ConfigPath string `type:"string" help:"Path to configuration file" default:"proxy.toml" arg:""`
}

type KeyserverCmd struct {
	ConfigPath string `type:"string" help:"Path to configuration file" default:"nks.toml" arg:""`
}

type LoadKeyCmd struct {
	CertPath   string `type:"string" help:"Path to the certificate to load" arg:""`
	KeyPath    string `type:"string" help:"Path to the key file" arg:""`
	ConfigPath string `type:"string" help:"Path to the configuration file" default:"nks.toml" arg:""`
}

type VersionCmd struct{}

var CLI struct {
	Proxy     ProxyCmd     `help:"Start a proxy" cmd:""`
	Keyserver KeyserverCmd `help:"Start a keyserver" cmd:""`
	LoadKey   LoadKeyCmd   `help:"Load a key" cmd:""`
	Version   VersionCmd   `help:"Print version" cmd:""`
}

func (c *ProxyCmd) Run(ctx *CLIContext) error {
	kttlog.Logger.Info().Msg("Starting proxy")
	p, err := proxy.NewSimpleProxy(c.ConfigPath)
	if err != nil {
		return err
	}

	return p.ListenAndServe()
}

func (c *KeyserverCmd) Run(ctx *CLIContext) error {
	kttlog.Logger.Info().Msg("Starting keyserver")

	nks, err := nokeyserver.NewNoKeyServer(c.ConfigPath)
	if err != nil {
		kttlog.Logger.Fatal().Err(err).Msg("Failed to start keyserver")
	}

	nks.Run()

	return nil
}

func (c *LoadKeyCmd) Run(ctx *CLIContext) error {
	return keyloader.LoadKeyWithConfig(c.CertPath, c.KeyPath, c.ConfigPath)
}

func (c *VersionCmd) Run(ctx *CLIContext) error {
	println("Version: " + version)

	return nil
}

func main() {
	ctx := kong.Parse(&CLI)

	debugLevel := os.Getenv("DEBUG")
	clictx := &CLIContext{}

	if debugLevel != "" {
		kttlog.SetLevelStr(debugLevel)
	}

	err := ctx.Run(clictx)
	ctx.FatalIfErrorf(err)
}
