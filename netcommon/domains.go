package netcommon

import (
	"regexp"
	"strings"
)

// GetWildcardIssuer will get the string representation of a potential wilcard issuer for
// a given domain. Note that this stops at top level domains, and will not, for example, return
// *.com for the given input of example.com. This uses a regex internally.
func GetWildcardIssuer(domain string) string {
	// BUG(space55): This will not function for two-level TLDs, such as .co.uk.
	if strings.Count(domain, ".") < 2 {
		return ""
	}
	re, _ := regexp.Compile(`^[^.]+\.`)
	return re.ReplaceAllString(domain, "*.")
}
