package netcommon

import (
	"os"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// DeferrableClose is a wrapper to close a file, while catching any potential errors and logging them.
func DeferrableClose(file *os.File) {
	if err := file.Close(); err != nil {
		kttlog.Logger.Error().Err(err).Msg("netcommon.DeferrableClose: Error closing file")
	}
}
