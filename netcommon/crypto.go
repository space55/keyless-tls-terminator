package netcommon

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/x509"
)

// Verify verifies that a given challenge matches a given signature. It returns any error encountered during processing. A nil error
// means the verification was successful.
func Verify(pub crypto.PublicKey, alg x509.PublicKeyAlgorithm, hashFunc crypto.Hash, sigType SigType, challenge []byte, signed []byte) error {
	switch alg {
	case x509.RSA:
		switch sigType {
		case SigType_RSA_PKCS1:
			return rsa.VerifyPKCS1v15(pub.(*rsa.PublicKey), hashFunc, challenge, signed)
		case SigType_RSA_PSS:
			return rsa.VerifyPSS(pub.(*rsa.PublicKey), hashFunc, challenge, signed, &rsa.PSSOptions{SaltLength: rsa.PSSSaltLengthEqualsHash})
		default:
			return &UnknownSigTypeError{SigType: sigType}
		}
	case x509.ECDSA:
		valid := ecdsa.VerifyASN1(pub.(*ecdsa.PublicKey), challenge, signed)
		if !valid {
			return &InvalidSignatureError{}
		}
		return nil
	case x509.Ed25519:
		valid := ed25519.Verify(pub.(ed25519.PublicKey), challenge, signed)
		if !valid {
			return &InvalidSignatureError{}
		}
		return nil
	default:
		return x509.ErrUnsupportedAlgorithm
	}
}
