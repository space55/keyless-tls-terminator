package netcommon_test

import (
	"testing"

	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

func TestDummyCertRSA(t *testing.T) {
	_, _, err := netcommon.GenerateDummyPairRSA([]string{"example.com"})
	if err != nil {
		t.Fatal(err)
	}
}

func TestDummyCertECDSA(t *testing.T) {
	_, _, err := netcommon.GenerateDummyPairECDSA([]string{"example.com"})
	if err != nil {
		t.Fatal(err)
	}
}

func TestDummyCertED25519(t *testing.T) {
	_, _, err := netcommon.GenerateDummyPairEd25519([]string{"example.com"})
	if err != nil {
		t.Fatal(err)
	}
}

func TestChain(t *testing.T) {
	t.Log("This function doesn't quite work right")
	return
	/*
		kttlog.SetLevel(zerolog.TraceLevel)
		cert, err := netcommon.LoadTLSCertFromPath("/tmp/dummy.chain.crt")
		if err != nil {
			t.Fatal(err)
		}

		t.Log("Got a certificate chain with ", len(cert.Certificate), " certificates")
	*/
}
