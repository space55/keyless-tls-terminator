package netcommon

import (
	"crypto/ecdsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"os"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// SaveECDSAPublicKey saves the ECDSA public key to the given path. It returns any error encountered during processing.
func SaveECDSAPublicKey(path string, key *ecdsa.PublicKey) error {
	kttlog.Logger.Info().Str("path", path).Msg("netcommon.SaveECDSAPublicKey: Saving public key to path")
	f, err := os.Create(path)
	if err != nil {
		return err
	}

	data, err := x509.MarshalPKIXPublicKey(key)
	if err != nil {
		return err
	}

	block := pem.Block{
		Type:  PEMHeaderECDSAPub,
		Bytes: data,
	}

	return pem.Encode(f, &block)
}

// SaveCert saves the certificate to the given path. It returns any error encountered during processing.
func SaveCert(certPath string, cert *x509.Certificate) error {
	kttlog.Logger.Info().Str("path", certPath).Msg("netcommon.SaveCert: Saving certificate to path")
	fCrt, err := os.Create(certPath)
	if err != nil {
		return err
	}

	block := pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cert.Raw,
	}
	return pem.Encode(fCrt, &block)

}

// SaveTLSCert saves the *tls.Certificate and private key to the given paths. It returns any error encountered during processing.
func SaveTLSCert(certPath, keypath string, cert *tls.Certificate) error {
	kttlog.Logger.Info().Str("path", certPath).Str("key_path", keypath).Msg("netcommon.SaveTLSCert: Saving TLS certificate to path")
	fCrt, err := os.Create(certPath)
	if err != nil {
		return err
	}
	fKey, err := os.Create(keypath)
	if err != nil {
		return err
	}

	for _, data := range cert.Certificate {
		block := pem.Block{
			Type:  "CERTIFICATE",
			Bytes: data,
		}
		err = pem.Encode(fCrt, &block)
		if err != nil {
			return err
		}
	}

	priv, err := x509.MarshalECPrivateKey(cert.PrivateKey.(*ecdsa.PrivateKey))
	if err != nil {
		return err
	}
	keyBlock := pem.Block{
		Type:  "ECDSA PRIVATE KEY",
		Bytes: priv,
	}
	err = pem.Encode(fKey, &keyBlock)
	if err != nil {
		return err
	}

	kttlog.Logger.Debug().Msg("netcommon.SaveTLSCert: Certificate saved")

	return nil
}

// SaveECDSAKey saves the ECDSA private key to the given path. It returns any error encountered during processing.
func SaveECDSAKey(keypath string, priv *ecdsa.PrivateKey) error {
	kttlog.Logger.Info().Str("path", keypath).Msg("netcommon.SaveECDSAKey: Saving ECDSA private key to path")
	fCrt, err := os.Create(keypath)
	if err != nil {
		return err
	}

	privBytes, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		return err
	}

	block := pem.Block{
		Type:  PEMHeaderECDSAPriv,
		Bytes: privBytes,
	}

	return pem.Encode(fCrt, &block)

}
