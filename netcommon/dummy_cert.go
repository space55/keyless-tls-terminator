package netcommon

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"math/big"
	"time"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func getTemplate(domains []string, pubAlg x509.PublicKeyAlgorithm, pub any) x509.Certificate {
	kttlog.Logger.Trace().Str("alg", pubAlg.String()).Msg("netcommon.getTemplate: Creating certificate template")
	return x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"Dummy Certificate Co"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 180),

		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign | x509.KeyUsageKeyEncipherment,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
		IsCA:                  true,
		PublicKey:             pub,
		PublicKeyAlgorithm:    pubAlg,
		DNSNames:              domains,
	}
}

func toTLSCertificate(data []byte, priv crypto.PrivateKey) (*tls.Certificate, error) {
	kttlog.Logger.Trace().Msg("netcommon.toTLSCertificate: Converting private key to TLS certificate")
	cert, err := x509.ParseCertificate(data)
	if err != nil {
		return nil, err
	}

	return &tls.Certificate{Certificate: [][]byte{data}, PrivateKey: priv, Leaf: cert}, nil
}

func certFromPair(domains []string, pubAlg x509.PublicKeyAlgorithm, pub crypto.PublicKey, priv crypto.PrivateKey) (*tls.Certificate, error) {
	kttlog.Logger.Trace().Str("alg", pubAlg.String()).Msg("netcommon.certFromPair: Creating certificate")
	template := getTemplate(domains, pubAlg, pub)
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)
	if err != nil {
		return nil, err
	}

	tlsCert, err := toTLSCertificate(derBytes, priv)
	if err != nil {
		return nil, err
	}

	return tlsCert, nil
}

// GenerateDummyPairRSA generates a dummy self-signed *tls.Certificate for a given list of domains, interpreted as
// SAN records. It returns the certificate, the private key, and any error that occurred.
func GenerateDummyPairRSA(domains []string) (*tls.Certificate, *rsa.PrivateKey, error) {
	kttlog.Logger.Debug().Strs("domains", domains).Msg("netcommon.GenerateDummyPairRSA: Generating RSA key pair")
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	cert, err := certFromPair(domains, x509.RSA, priv.Public(), priv)
	return cert, priv, err
}

// GenerateDummyPairECDSA generates a dummy self-signed *tls.Certificate for a given list of domains, interpreted as
// SAN records. It returns the certificate, the private key, and any error that occurred.
func GenerateDummyPairECDSA(domains []string) (*tls.Certificate, *ecdsa.PrivateKey, error) {
	kttlog.Logger.Debug().Strs("domains", domains).Msg("netcommon.GenerateDummyPairECDSA: Generating ECDSA key pair")
	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, nil, err
	}

	cert, err := certFromPair(domains, x509.ECDSA, priv.Public(), priv)
	return cert, priv, err
}

// GenerateDummyPairEd25519 generates a dummy self-signed *tls.Certificate for a given list of domains, interpreted as
// SAN records. It returns the certificate, the private key, and any error that occurred.
func GenerateDummyPairEd25519(domains []string) (*tls.Certificate, *ed25519.PrivateKey, error) {
	kttlog.Logger.Debug().Strs("domains", domains).Msg("netcommon.GenerateDummyPairEd25519: Generating Ed25519 key pair")
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return nil, nil, err
	}

	cert, err := certFromPair(domains, x509.Ed25519, pub, priv)
	return cert, &priv, err
}

// GenerateDummyCert generates a dummy self-signed *tls.Certificate for a given list of domains, interpreted as
// SAN records. The certificate it generates will be using ECDSA. It does _not_ return the private key.
func GenerateDummyCert(domains []string) (*tls.Certificate, error) {
	kttlog.Logger.Trace().Strs("domains", domains).Msg("netcommon.GenerateDummyCert: Generating dummy certificate")
	cert, _, err := GenerateDummyPairECDSA(domains)
	return cert, err
}
