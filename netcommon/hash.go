package netcommon

import "crypto/sha256"

// String gets the string representation for a given SigType
func (s SigType) String() string {
	switch s {
	case SigType_DEFAULT:
		return "DEFAULT"
	case SigType_RSA_PKCS1:
		return "RSA_PKCS1"
	case SigType_RSA_PSS:
		return "RSA_PSS"
	case SigType_ECDSA:
		return "ECDSA"
	case SigType_Ed25519:
		return "Ed25519"
	default:
		return "unknown"
	}
}

// ComputeHashSHA256 is a helper function that returns a slice representation of the SHA256 hash of the given input.
func ComputeHashSHA256(data []byte) []byte {
	sum := sha256.Sum256(data)
	return sum[:]
}
