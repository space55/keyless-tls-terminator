package netcommon

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func parseCertificate(data []byte) (*x509.Certificate, []byte, error) {
	block, rest := pem.Decode(data)
	if block == nil {
		return nil, rest, &x509.CertificateInvalidError{}
	}

	ca, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, rest, err
	}

	return ca, rest, nil
}

// ReadCAPath reads a CA from a given path, and adds it to the given certificate pool.
func ReadCAPath(path string, certPool *x509.CertPool) error {
	remoteCAContents, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	for len(remoteCAContents) > 0 {
		var ca *x509.Certificate
		ca, remoteCAContents, err = parseCertificate(remoteCAContents)
		if err != nil {
			return err
		}
		certPool.AddCert(ca)
	}

	return nil
}

// LoadCertFromPath reads an x509 certificate from a given path.
func LoadCertFromPath(path string) (*x509.Certificate, error) {
	kttlog.Logger.Trace().Str("path", path).Msg("netcommon.LoadCertFromPath: Reading certificate from path")
	f, err := os.Open(path)
	defer DeferrableClose(f)
	if err != nil {
		return nil, err
	}

	return UnmarshalPEMToCert(f)
}

// LoadTLSCertFrommPath creates a *tls.Certificate struct from a given path
func LoadTLSCertFromPath(path string) (*tls.Certificate, error) {
	kttlog.Logger.Trace().Str("path", path).Msg("netcommon.LoadTLSCertFromPath: Reading TLScertificate from path")
	f, err := os.Open(path)
	defer DeferrableClose(f)
	if err != nil {
		return nil, err
	}

	cert, err := UnmarshalPEMToTLSCert(f)
	if err != nil {
		return nil, err
	}
	if cert == nil {
		return nil, &x509.CertificateInvalidError{Detail: "No valid certificates found at " + path}
	}

	kttlog.Logger.Debug().Str("cn", cert.Leaf.Subject.CommonName).Str("path", path).Strs("domains", cert.Leaf.DNSNames).Msg("netcommon.LoadTLSCertFromPath: Loaded certificate")
	return cert, nil
}

// LoadKeyPair loads a private key and certificate from a given path into a *tls.Certificate struct.
func LoadKeyPair(certPath, keyPath string) (*tls.Certificate, error) {
	kttlog.Logger.Trace().Str("path", certPath).Str("key_path", keyPath).Msg("netcommon.LoadKeyPair: Reading certificate from path")
	cert, err := tls.LoadX509KeyPair(certPath, keyPath)
	if err != nil {
		return nil, err
	}

	return &cert, nil
}

// ReadBytes essentially gets the bytes from an io.Reader. It is a helper function used elsewhere, to reduce
// code duplication.
func ReadBytes(in io.Reader) ([]byte, error) {
	buf := &bytes.Buffer{}
	_, err := buf.ReadFrom(in)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// ReadPem reads a PEM block from an io.Reader, and returns the decoded data.
// It will discard any remaining data after reading the first PEM block.
func ReadPem(in io.Reader) (*pem.Block, error) {
	b, _, err := ReadPemWithRest(in)
	return b, err
}

// ReadPemWithRest reads a PEM block from an io.Reader, and returns the decoded data, and any remaining data.
func ReadPemWithRest(in io.Reader) (*pem.Block, []byte, error) {
	buf, err := ReadBytes(in)
	if err != nil {
		return nil, nil, err
	}
	b, rest := pem.Decode(buf)
	return b, rest, nil
}

// WritePem saves a PEM block to an io.Writer, based on the blockType specified in the call.
func WritePem(out io.Writer, blockType string, data []byte) error {
	return pem.Encode(out, &pem.Block{Type: blockType, Bytes: data})
}

// MarshalPrivateKeyToPEM creates a PEM block from a given private key, using the specified
// algorithm, and writes it to the specified io.Writer. It will return any error encountered
// during the process. Note that this function uses the x509.MarshalPKCS8PrivateKey function,
// and therefore outputs keys in PKCS8 format.
func MarshalPrivateKeyToPEM(out io.Writer, alg x509.PublicKeyAlgorithm, priv any) error {
	kttlog.Logger.Debug().Msg("netcommon.MarshalPrivateKeyToPEM: Writing private key")

	var data []byte
	var err error
	var blockType string
	switch alg {
	case x509.RSA:
		blockType = PEMHeaderDefaultPriv
		data, err = x509.MarshalPKCS8PrivateKey(priv.(*rsa.PrivateKey))
	case x509.ECDSA:
		blockType = PEMHeaderDefaultPriv
		data, err = x509.MarshalPKCS8PrivateKey(priv.(*ecdsa.PrivateKey))
	case x509.Ed25519:
		blockType = PEMHeaderDefaultPriv
		data, err = x509.MarshalPKCS8PrivateKey(priv.(*ed25519.PrivateKey))
	}

	if err != nil {
		return err
	}

	return pem.Encode(out, &pem.Block{Type: blockType, Bytes: data})
}

// MarshalCertToPEM creates a PEM block from a given certificate, and writes it to the specified io.Writer.
func MarshalCertToPEM(out io.Writer, cert *x509.Certificate) error {
	kttlog.Logger.Debug().Str("cert_cn", cert.Subject.CommonName).Msg("netcommon.MarshalCertToPem: Writing certificate")
	return pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Raw})
}

// UnmarshalPEMToPrivateKey reads from the specified io.Reader, and attempts to unmarshal the data into a private key.
// It will return any error it encounters during the processing, alongside the *crypto.PrivateKey object.
// Note: This function uses the x509.ParsePKCS8PrivateKey function internally, so any private keys that can be read
// from the io.Reader must be in the PKCS8 format.
func UnmarshalPEMToPrivateKey(in io.Reader) (*crypto.PrivateKey, error) {
	kttlog.Logger.Debug().Msg("netcommon.UnmarshalPEMToPrivateKey: Reading private key")
	data, err := ReadPem(in)
	if err != nil {
		return nil, err
	}

	keyAny, err := x509.ParsePKCS8PrivateKey(data.Bytes)
	if err != nil {
		return nil, err
	}

	return keyAny.(*crypto.PrivateKey), nil
}

// UnmarshalPEMToCert will read from the specified io.Reader, and attempt to unmarshal the data into a certificate.
// It will return any error encountered during processing, alongside the *x509.Certificate struct.
func UnmarshalPEMToCert(in io.Reader) (*x509.Certificate, error) {
	kttlog.Logger.Debug().Msg("netcommon.UnmarshalPEMToCert: Reading certificate")
	data, err := ReadPem(in)
	if err != nil {
		return nil, err
	}

	cert, err := x509.ParseCertificate(data.Bytes)
	if err != nil {
		return nil, err
	}

	return cert, nil
}

// UnmarshalPEMToTLSCert will read from the io.Reader, and attempt to marshal the data into a *tls.Certificate.
// It will return any error it encounters during processing.
func UnmarshalPEMToTLSCert(in io.Reader) (*tls.Certificate, error) {
	kttlog.Logger.Debug().Msg("netcommon.UnmarshalPEMToTLSCert: Reading TLS certificate")
	data, rest, err := ReadPemWithRest(in)

	cert := &tls.Certificate{}
	for len(rest) != 0 || data != nil {
		if err != nil {
			return nil, err
		}

		if data == nil {
			kttlog.Logger.Trace().Int("len", len(rest)).Msg("netcommon.UnmarshalPEMToTLSCert: No more data")
			break
		}

		parsed, err := x509.ParseCertificate(data.Bytes)
		if err != nil {
			return nil, err
		}
		cert.Certificate = append(cert.Certificate, parsed.Raw)
		if cert.Leaf == nil {
			cert.Leaf = parsed
		}
		kttlog.Logger.Trace().Str("cert_cn", parsed.Subject.CommonName).Int("len_left", len(rest)).Msg("netcommon.UnmarshalPEMToTLSCert: Read certificate")

		data, rest = pem.Decode(rest)
	}

	return cert, nil
}

// NewCertPool creates an *x509.CertPool from a list of specified certificates. This is useful for using
// the native Golang x509 certificate validation, as it can be efficiently checked to see if a certificate
// is valid.
func NewCertPool(certs []string) (*x509.CertPool, error) {
	pool := x509.NewCertPool()
	for _, path := range certs {
		err := ReadCAPath(path, pool)
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("netcommon.NewCertPool: Error creating new cert pool")
		}
	}
	return pool, nil
}
