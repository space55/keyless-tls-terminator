package netcommon_test

import (
	"testing"

	"github.com/rs/zerolog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

func TestDomainWildcard(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	testA := "example.com"
	testB := "www.example.com"
	testC := "abc.www.example.com"
	testD := "xyz.abc.www.example.com"

	if netcommon.GetWildcardIssuer(testA) == "*.com" {
		t.Fatal("example.com failed")
	}
	if netcommon.GetWildcardIssuer(testB) != "*.example.com" {
		t.Fatal("www.example.com failed")
	}
	if netcommon.GetWildcardIssuer(testC) != "*.www.example.com" {
		t.Fatal("abc.www.example.com failed")
	}
	if netcommon.GetWildcardIssuer(testD) != "*.abc.www.example.com" {
		t.Fatal("xyz.abc.www.example.com failed")
	}
	if netcommon.GetWildcardIssuer(testD) == "*.example.com" {
		t.Fatal("xyz.abc.www.example.com failed going too far up the tree")
	}
	if netcommon.GetWildcardIssuer(testD) == "example.com" {
		t.Fatal("xyz.abc.www.example.com failed... by generating a non-wildcard???")
	}
}
