package netcommon

import (
	"crypto"
	"io"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// Public gets the public key associated with a RemotePrivateKey.
func (k RemotePrivateKey) Public() crypto.PublicKey {
	return k.PublicKey
}

// Sign signs the given data with the private key on the NKS server. It returns the signature,
// and any error that occurred. It calls the RemoteProofFunc function internally.
func (k RemotePrivateKey) Sign(rand io.Reader, data []byte, opts crypto.SignerOpts) ([]byte, error) {
	proof, err := k.RemoteProofFunc(opts.HashFunc(), k.SigType, data)
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("RemotePrivateKey.Sign: Failed to sign data")
	}

	kttlog.Logger.Trace().Int("len", len(proof)).Msg("RemotePrivateKey.Sign: Successfully signed proof")

	return proof, err
}
