// netcommon is a suite of common utilities that are used throughout the ecosystem. It mainly
// handles key and certificate parsing, storage, and marshalling, but has some extra functionality
// that has been needed by both the clients and the servers.
package netcommon

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
)

const PEMHeaderCertificate = "CERTIFICATE"
const PEMHeaderDefaultPriv = "PRIVATE KEY"
const PEMHeaderPub = "PUBLIC KEY"
const PEMHeaderRSAPriv = "RSA PRIVATE KEY"
const PEMHeaderRSAPub = "PUBLIC KEY"
const PEMHeaderECDSAPriv = "EC PRIVATE KEY"
const PEMHeaderECDSAPub = "PUBLIC KEY"
const PEMHeaderEd25519Priv = "ED25519 PRIVATE KEY"
const PEMHeaderEd25519Pub = "PUBLIC KEY"

// SigType is the type of signature used to sign a challenge. It is used over the wire to identify the type of signature required for the NKS.
type SigType uint8

const (
	SigType_DEFAULT SigType = iota
	SigType_RSA_PKCS1
	SigType_RSA_PSS
	SigType_ECDSA
	SigType_Ed25519
)

// RemoteProofFunc is a function that performs a proof on a remote server, with the given parameters. It returns the proof, and any error encountered.
type RemoteProofFunc func(crypto.Hash, SigType, []byte) ([]byte, error)

// RemotePrivateKey is a wrapper around a crypto.Signer, which enables the private key to be stored on a remote server, and never accessed by
// the application that stores the RemotePrivateKey struct, while still allowing signature operations to take place.
type RemotePrivateKey struct {
	// Domain is the domain for which this RemotePrivateKey is associated.
	Domain string
	crypto.Signer
	// RemoteProofFunc is the function that is called whenever it is asked to sign a challenge.
	RemoteProofFunc RemoteProofFunc
	// PublicKey is the public key associated with the private key stored in the NKS
	PublicKey       crypto.PublicKey
	// RemoteCert is an x509.Certificate that contains the public key associated with the private key stored in the NKS
	RemoteCert      *x509.Certificate
	// RemoteTLSCert is a tls.Certificate that contains the public key associated with the private key stored in the NKS. It does _not_ contain the private key,
	// and as such, cannot be used to sign challenges.
	RemoteTLSCert   *tls.Certificate
	// SigType is the type of signature used to sign a challenge. It is used over the wire to identify the type of signature required for the NKS.
	SigType         SigType
}

// InvalidCAPathError is an error that is returned when the CA path is invalid.
type InvalidCAPathError struct {
	path string
}

// Error returns the string representation of an InvalidCAPathError struct.
func (e *InvalidCAPathError) Error() string {
	return "invalid CA path: " + e.path
}

// UnknownSigTypeError is an error that is returned when the signature type is unknown.
type UnknownSigTypeError struct {
	SigType SigType
}

// Error returns the string representation of an UnknownSigTypeError struct.
func (e *UnknownSigTypeError) Error() string {
	return "unknown sig type: " + e.SigType.String()
}

// InvalidSignatureError is an error that is returned when the signature is invalid.
type InvalidSignatureError struct{}

// Error returns the string representation of an InvalidSignatureError struct.
func (e *InvalidSignatureError) Error() string {
	return "invalid signature"
}
