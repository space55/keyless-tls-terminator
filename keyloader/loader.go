//keyloader is a simple package that loads keys into a datastore from a pair of files.
package keyloader

import (
	"bytes"

	"gitlab.com/space55/keyless-tls-terminator/datastore"
	"gitlab.com/space55/keyless-tls-terminator/keystore"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/nokeyserver"
)

// LoadKeyWithConfig loads a key and certificate from a given path, and stores it in the datastore specified in an NKS
// config file provided in configPath. It returns an error if any of the steps fail.
func LoadKeyWithConfig(crtPath, keyPath, configPath string) error {
	kttlog.Logger.Info().Str("path", configPath).Str("key_path", keyPath).Str("cert_path", crtPath).Msg("keyloader.LoadKeyWithConfig: Loading key with config")
	s, err := nokeyserver.NewNoKeyServer(configPath)
	if err != nil {
		return err
	}

	return LoadKey(crtPath, keyPath, s.Datastore)
}

// LoadKey loads a key and certificate from a given path, and stores it in the datastore specified in the arguments
func LoadKey(crtPath string, keyPath string, ds datastore.Datastore) error {
	kttlog.Logger.Debug().Str("path", crtPath).Str("key_path", keyPath).Msg("keyloader.LoadKey: Loading key")
	key, err := keystore.NewKeyFromFile(crtPath, keyPath)
	if err != nil {
		return err
	}

	certBuf := &bytes.Buffer{}
	err = netcommon.MarshalCertToPEM(certBuf, key.GetCertificate())
	if err != nil {
		return err
	}

	keyBuf := &bytes.Buffer{}
	err = netcommon.MarshalPrivateKeyToPEM(keyBuf, key.GetAlgorithm(), key.GetPrivateKey())
	if err != nil {
		return err
	}

	for _, domain := range key.GetTLSCertificate().Leaf.DNSNames {
		err = ds.SaveCertificate(domain, certBuf.Bytes())
		if err != nil {
			return err
		}
		err = ds.SavePrivateKey(domain, keyBuf.Bytes())
		if err != nil {
			return err
		}
	}

	return nil
}
