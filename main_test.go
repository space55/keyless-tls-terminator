package main_test

import (
	"crypto/tls"
	"crypto/x509"
	"io"
	"testing"

	"github.com/rs/zerolog"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/nokeyserver"
	"gitlab.com/space55/keyless-tls-terminator/proxy"
)

type proxyServer struct {
	p       *proxy.SimpleProxy
	t       *testing.T
	errChan chan error
	io.Closer
}

func (p *proxyServer) Close() error {
	return p.p.Close()
}

func (p *proxyServer) Run() {
	err := p.p.ListenAndServe()
	p.errChan <- err
}

func TestMain(t *testing.T) {
	t.Log("Running TestMain")
	kttlog.SetLevel(zerolog.TraceLevel)

	dummyCert, dummyKey, err := netcommon.GenerateDummyPairECDSA([]string{"localhost"})
	if err != nil {
		t.Fatal(err)
	}

	err = netcommon.SaveCert("/tmp/dummy.crt", dummyCert.Leaf)
	if err != nil {
		t.Fatal(err)
	}
	err = netcommon.SaveECDSAKey("/tmp/dummy.key", dummyKey)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Generated keys")

	caPool := x509.NewCertPool()
	caPool.AddCert(dummyCert.Leaf)

	nks, err := nokeyserver.NewNoKeyServerFromParams("tcp4", "localhost:7753", "/tmp/dummy.crt", "/tmp/dummy.key", []string{"/tmp/dummy.crt"})
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Created NoKeyServer")

	err = nks.LoadKeyFromFile("/tmp/dummy.crt", "/tmp/dummy.key")
	if err != nil {
		t.Fatal(err)
	}

	go nks.Run()
	t.Log("Running NKS")

	p := &proxyServer{
		t:       t,
		errChan: make(chan error),
	}

	p.p = proxy.NewSimpleProxyFromParams("file", "/dev/null", "tcp4", "localhost:1443", "tcp4", "localhost:7753", "/tmp/dummy.crt", "/tmp/dummy.key", []string{"/tmp/dummy.crt"})

	p.p.OutputMasterKeysToFile("/tmp/masterkeys.txt")
	p.p.LoadRemoteCerts([]string{"/tmp/dummy.crt"})

	p.p.Prepare()

	go p.Run()

	clientConf := &tls.Config{
		InsecureSkipVerify: true,
	}

	c, err := tls.Dial("tcp4", "localhost:1443", clientConf)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Dialled localhost:1443")

	_, err = c.Write([]byte("GET / HTTP/1.0\r\n\r\n"))
	if err != nil {
		t.Fatal(err)
	}

	err = c.Close()
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Closed client")

	err = p.Close()
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Closed proxy")

	nks.Close()

	t.Log("Stopped NKS")

	err = <-p.errChan
	if err != nil {
		t.Fatal(err)
	}
}
