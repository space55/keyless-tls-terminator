package server

import (
	"crypto/tls"
	"crypto/x509"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func NewServer(network, laddr string, messageHandler MessageHandler, cert tls.Certificate, rootCAs x509.CertPool) *Server {
	kttlog.Logger.Info().Msg("server.NewServer: Creating new server")
	cfg := &tls.Config{
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
	}
	s := &Server{
		network:        network,
		laddr:          laddr,
		MessageHandler: messageHandler,
		cfg:            cfg,
		stopChan:       make(chan int),
		rootCAs:        &rootCAs,
		wlock:          &sync.Mutex{},
		closeGroup:     &sync.WaitGroup{},
		blockingChan:   make(chan int),
		shouldBlock:    false,
	}

	return s
}

func (s *Server) StartBlockingUntilReady() {
	s.shouldBlock = true
	s.Start()
	<-s.blockingChan
}

func (s *Server) Start() {
	s.prepServer()
	go s.loop()
}

func (s *Server) StartBlocking() {
	s.prepServer()
	s.loop()
}

func (s *Server) Close() {
	kttlog.Logger.Info().Msg("Server.Close: Closing server")
	err := s.internalLn.Close()
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("Server.Close: Failed to close server")
	}

	s.closeGroup.Add(len(s.conns))
	for _, sc := range s.conns {
		go sc.Close(s.onSCClose)
	}
}

func (s *Server) Wait() {
	kttlog.Logger.Info().Msg("Server.Wait: Waiting for server to close")

	s.closeGroup.Wait()

	for {
		code := <-s.stopChan
		switch code {
		case -1:
			kttlog.Logger.Info().Msg("Server.Wait: Server closed")
			return
		default:
			kttlog.Logger.Debug().Int("code", code).Msg("Server.Wait: Unknown code")
		}
	}
}

func (s *Server) SetOnReady(onReady func()) {
	s.onReady = onReady
}
