package server

import (
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func GetDummyHandler() func(ResponseCallback, *proto.Message) error {
	return func(respHandler ResponseCallback, msg *proto.Message) error {
		for _, b := range msg.GetOps() {
			switch b.GetType() {
			case 0xff:
				kttlog.Logger.Info().Uint32("id", uint32(b.GetID())).Str("debug_msg", string(b.GetContent())).Msg("server.GetDummyHandler: Received debug message from remote")
			default:
				kttlog.Logger.Warn().Uint32("op_id", uint32(b.GetID())).Uint8("op_type", uint8(b.GetType())).Msg("server.GetDummyHandler: Received unknown op")
			}
		}

		return nil
	}
}
