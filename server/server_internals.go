package server

import (
	"crypto/tls"
	"errors"
	"net"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func (s *Server) prepServer() {
	kttlog.Logger.Info().Str("addr", s.laddr).Msg("Server.prepServer: Preparing server")

	kttlog.Logger.Trace().Int("cert_len", len(s.cfg.Certificates)).Msg("Server.prepServer: Found TLS certificates")
	if len(s.cfg.Certificates) == 0 {
		kttlog.Logger.Fatal().Msg("Server.prepServer: No TLS certificates provided")
		return
	}

	var err error
	s.internalLn, err = tls.Listen(s.network, s.laddr, s.cfg)
	if err != nil {
		kttlog.Logger.Fatal().Err(err).Msg("Server.prepServer: Failed to listen")
	}
}

func (s *Server) onSCClose() {
	s.closeGroup.Done()
}

func (s *Server) done() {
	kttlog.Logger.Trace().Msg("Server.done: Sending done signal")
	s.stopChan <- -1
	kttlog.Logger.Trace().Msg("Server.done: Server sent done signal")
}

func (s *Server) loop() {
	defer s.done()
	if s.onReady != nil {
		kttlog.Logger.Debug().Msg("Server.loop: Calling s.OnReady")
		s.onReady()
	}
	if s.shouldBlock {
		s.blockingChan <- 1
	}

	kttlog.Logger.Trace().Msg("Server.loop: Starting")

	for {
		conn, err := s.internalLn.Accept()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				kttlog.Logger.Trace().Msg("Server.loop: Server conn closed")
				return
			}
			kttlog.Logger.Error().Err(err).Msg("Server.loop: Error while accepting new connection")
			continue
		}
		kttlog.Logger.Trace().Str("addr", conn.RemoteAddr().String()).Msg("Server.loop: Accepted new connection")
		go s.handleConnection(conn)
	}
}
