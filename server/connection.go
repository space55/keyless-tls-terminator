package server

import (
	"bufio"
	"io"
	"net"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func (s *Server) handleConnection(conn net.Conn) {
	msgBuf := proto.NewMessage()
	sc := &ServerConn{
		Conn:           conn,
		MessageHandler: s.MessageHandler,
		mutex:          &sync.Mutex{},
		messageBuf:     msgBuf,
		copyMutex:      &sync.Mutex{},
		writeMutex:     &sync.Mutex{},
	}

	s.wlock.Lock()
	s.conns = append(s.conns, sc)
	s.wlock.Unlock()
	go sc.messageLoop()
	sc.Run()
}

func (sc *ServerConn) Run() {
	r := bufio.NewReader(sc.Conn)
	for {
		msg, err := proto.ReadFrom(r)

		if err != nil && err != io.EOF {
			if !sc.closed {
				kttlog.Logger.Error().Err(err).Msg("ServerConn.Run: Error reading from connection")
				continue
			}
			return
		}

		if msg != nil {
			kttlog.Logger.Debug().Uint32("id", uint32(msg.GetID())).Msg("ServerConn.Run: Received message")
			if sc.MessageHandler != nil {
				kttlog.Logger.Trace().Uint32("id", uint32(msg.GetID())).Msg("ServerConn.Run: Handing message off to messageHandler")
				err := sc.MessageHandler(sc.responseHandler, msg)
				if err != nil {
					kttlog.Logger.Error().Err(err).Msg("ServerConn.Run: Error handling message")
					continue
				}
			}
		}

		if err == io.EOF {
			kttlog.Logger.Debug().Msg("ServerConn.Run: Closing handleConnection (EOF)")
			return
		}
	}
}

func (sc *ServerConn) responseHandler(op *proto.Op) error {
	kttlog.Logger.Debug().Uint32("id", uint32(op.GetID())).Msg("ServerConn.responseHandler: Sending op")
	return sc.WriteOp(op)
}

func (sc *ServerConn) Close(onClose func()) {
	defer onClose()
	sc.closed = true
	kttlog.Logger.Debug().Msg("ServerConn.Close: Closing connection")
	err := sc.Conn.Close()
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("ServerConn.Close: Error closing connection")
	}

	sc.mutex.Lock()
}
