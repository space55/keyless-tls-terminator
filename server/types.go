package server

import (
	"crypto/tls"
	"crypto/x509"
	"net"
	"sync"
	"time"

	"gitlab.com/space55/keyless-tls-terminator/proto"
)

const loopDelay = time.Millisecond

type Server struct {
	network        string
	laddr          string
	internalLn     net.Listener
	cfg            *tls.Config
	stopChan       chan int
	MessageHandler MessageHandler
	onReady        func()
	wlock          *sync.Mutex
	conns          []*ServerConn
	closeGroup     *sync.WaitGroup
	rootCAs        *x509.CertPool
	blockingChan   chan int
	shouldBlock    bool
}

type ServerConn struct {
	Conn           net.Conn
	mutex          *sync.Mutex
	MessageHandler MessageHandler
	closed         bool
	copyMutex      *sync.Mutex
	writeMutex     *sync.Mutex
	messageBuf     *proto.Message
}

type ResponseCallback func(op *proto.Op) error
type MessageHandler func(responseHandler ResponseCallback, msg *proto.Message) error
