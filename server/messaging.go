package server

import (
	"time"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func (c *ServerConn) messageLoop() {
	for {
		time.Sleep(loopDelay)
		ops := c.messageBuf.GetOps()
		if len(ops) == 0 {
			continue
		}

		c.copyMutex.Lock()
		oldMsg := c.messageBuf
		c.messageBuf = proto.NewMessage()
		c.copyMutex.Unlock()

		err := c.WriteMessageDirect(oldMsg)
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("ServerConn.messageLoop: Error writing message")
			continue
		}

		kttlog.Logger.Trace().Int("op_len", len(ops)).Msg("ServerConn.messageLoop: Wrote message to remote")
	}
}

func (c *ServerConn) WriteOp(b *proto.Op) error {
	c.copyMutex.Lock()
	defer c.copyMutex.Unlock()

	return c.messageBuf.Push(b)
}

func (c *ServerConn) WriteMessageDirect(msg *proto.Message) error {
	c.writeMutex.Lock()
	defer c.writeMutex.Unlock()

	_, err := msg.WriteTo(c.Conn)

	return err
}
