module gitlab.com/space55/keyless-tls-terminator

go 1.18

require (
	github.com/BurntSushi/toml v1.1.0
	github.com/alecthomas/kong v0.5.0
	github.com/bradfitz/gomemcache v0.0.0-20220106215444-fb4bf637b56d
	github.com/go-redis/redis/v8 v8.11.5
	github.com/google/uuid v1.3.0
	github.com/juliangruber/go-intersect v1.1.0
	github.com/kataras/muxie v1.1.2
	github.com/rs/zerolog v1.26.1
	github.com/tchap/go-patricia/v2 v2.3.1
	github.com/vmihailenco/msgpack/v5 v5.3.5
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
)
