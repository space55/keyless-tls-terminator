package client

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"io"
	"strings"
	"sync"
	"time"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func NewClient(raddr string, cert *tls.Certificate, caPool *x509.CertPool, messageHandler MessageHandler) *Client {
	kttlog.Logger.Trace().Str("addr", raddr).Msg("client.NewClient: Creating new client")
	msg := proto.NewMessage()
	c := &Client{
		raddr:          raddr,
		cert:           cert,
		writeMutex:     &sync.Mutex{},
		copyMutex:      &sync.Mutex{},
		messageBuf:     msg,
		rootCAs:        caPool,
		caLock:         &sync.Mutex{},
		MessageHandler: messageHandler,
		LoopDelay:      time.Millisecond,
	}

	return c
}

func (c *Client) Connect() error {
	config := &tls.Config{
		Certificates: []tls.Certificate{*c.cert},
		RootCAs:      c.rootCAs,
		MinVersion:   tls.VersionTLS12,
	}

	kttlog.Logger.Debug().Str("addr", c.raddr).Msg("Client.Connect: Connecting to server")

	conn, err := tls.Dial("tcp", c.raddr, config)
	if err != nil {
		return err
	}

	kttlog.Logger.Debug().Str("addr", c.raddr).Msg("Client.Connect: Connected to server")
	c.conn = conn

	go c.readLoop()
	go c.writeLoop()

	kttlog.Logger.Info().Str("addr", c.raddr).Msg("Client.Connect: Connected")
	return nil
}

func (c *Client) Disconnect() error {
	if c.conn != nil {
		return c.conn.Close()
	}
	return nil
}

func (c *Client) WriteOp(b *proto.Op) error {
	c.copyMutex.Lock()
	defer c.copyMutex.Unlock()
	return c.messageBuf.Push(b)
}

func (c *Client) WriteMessageDirect(msg *proto.Message) error {
	kttlog.Logger.Debug().Int("len", len(msg.GetOps())).Uint32("msg_id", uint32(msg.GetID())).Msg("Client.WriteMessageDirect: Writing message")
	c.writeMutex.Lock()
	defer c.writeMutex.Unlock()

	_, err := msg.WriteTo(c.conn)

	kttlog.Logger.Debug().Int("len", len(msg.GetOps())).Uint32("msg_id", uint32(msg.GetID())).Msg("Client.WriteMessageDirect: Message written")

	return err
}

func (c *Client) readLoop() {
	kttlog.Logger.Info().Msg("Client.readLoop: Starting")
	r := bufio.NewReader(c.conn)
	for {
		msg, err := proto.ReadFrom(r)

		if err != nil && err != io.EOF {
			if strings.Contains(err.Error(), "use of closed network connection") {
				kttlog.Logger.Info().Msg("Client.readLoop: Connection closed")
				return
			}
			kttlog.Logger.Error().Err(err).Msg("Error handling connection")
			return
		}

		if msg != nil {
			kttlog.Logger.Info().Uint32("msg_id", uint32(msg.GetID())).Msg("Client.readLoop: Received message")
			c.MessageHandler(msg)
		}

		if err == io.EOF {
			kttlog.Logger.Info().Msg("Client.readLoop: Connection closed")
			return
		}
	}
}

func (c *Client) writeLoop() {
	kttlog.Logger.Info().Msg("Client.writeLoop: Starting")
	for {
		time.Sleep(c.LoopDelay)
		c.writeMutex.Lock()
		if len(c.messageBuf.GetOps()) == 0 {
			c.writeMutex.Unlock()
			continue
		}

		c.copyMutex.Lock()
		oldMsg := c.messageBuf
		c.messageBuf = proto.NewMessage()
		c.copyMutex.Unlock()
		c.writeMutex.Unlock()

		err := c.WriteMessageDirect(oldMsg)
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("Client.writeLoop: Error writing message")
		}

		kttlog.Logger.Trace().Int("len", len(oldMsg.GetOps())).Msg("Client.writeLoop: Wrote message")
	}
}
