package client

import (
	"crypto/tls"
	"crypto/x509"
	"sync"
	"time"

	"gitlab.com/space55/keyless-tls-terminator/proto"
)

type MessageHandler func(*proto.Message)

type Client struct {
	raddr          string
	conn           *tls.Conn
	cert           *tls.Certificate
	writeMutex     *sync.Mutex
	messageBuf     *proto.Message
	copyMutex      *sync.Mutex
	rootCAs        *x509.CertPool
	caLock         *sync.Mutex
	MessageHandler MessageHandler
	LoopDelay      time.Duration
}
