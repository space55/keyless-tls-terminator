package keystore

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"os"
	"path/filepath"

	"github.com/google/uuid"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

// SavePEM saves a PEM block to a file. It returns any error encountered while creating the file, or encoding the PEM data.
func SavePEM(path string, block *pem.Block) error {
	kttlog.Logger.Debug().Str("path", path).Str("header", block.Type).Msg("Key.SavePEM: Saving PEM")
	if path == "" {
		return nil
	}

	f, err := os.Create(path)
	defer netcommon.DeferrableClose(f)
	if err != nil {
		return err
	}

	return pem.Encode(f, block)
}

// SavePEMBytes saves the given PEM-encoded byte slice to the given path, with the given header. It will return
// any error encountered while creating the file, or encoding the PEM data.
func SavePEMBytes(path string, pemType string, data []byte) error {
	kttlog.Logger.Trace().Str("path", path).Str("header", pemType).Msg("Key.SavePEMBytes: Saving PEM")
	block := &pem.Block{
		Type:  pemType,
		Bytes: data,
	}

	return SavePEM(path, block)
}

// SaveCert saves the given certificate to the given path. It will return any error encountered while creating the file,
// or encoding the certificate to PEM.
func SaveCert(certPath string, cert *x509.Certificate) error {
	kttlog.Logger.Trace().Str("path", certPath).Str("cert_cn", cert.Subject.CommonName).Msg("Key.SaveCert: Saving cert")
	return SavePEMBytes(certPath, netcommon.PEMHeaderCertificate, cert.Raw)
}

// SaveRSAPrivateKey saves the given *rsa.PrivateKey to the given path. It will return any error encountered while creating the file,
// or encoding the key to PEM-encoded PKCS1 format.
func SaveRSAPrivateKey(keyPath string, priv *rsa.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Msg("Key.SaveRSAPrivateKey: Saving RSA private key")
	return SavePEMBytes(keyPath, netcommon.PEMHeaderRSAPriv, x509.MarshalPKCS1PrivateKey(priv))
}

// SaveECDSAPrivateKey saves the given *ecdsa.PrivateKey to the given path. It will return any error encountered while creating the file,
// or encoding the key to whatever PEM-encoded format Golang uses for the x509.MarshalECPrivateKey function.
func SaveECDSAPrivateKey(keyPath string, priv *ecdsa.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Msg("Key.SaveECDSAPrivateKey: Saving ECDSA private key")
	marshalled, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		return err
	}
	return SavePEMBytes(keyPath, netcommon.PEMHeaderECDSAPriv, marshalled)
}

// SaveEd25519PrivateKey saves the given *ed25519.PrivateKey to the given path. It will return any error encountered while creating the file,
// or encoding the key to PEM format.
func SaveEd25519PrivateKey(keyPath string, priv *ed25519.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Msg("Key.SaveEd25519PrivateKey: Saving Ed25519 private key")
	return SavePEMBytes(keyPath, netcommon.PEMHeaderEd25519Priv, priv.Seed())
}

// SaveRSA saves an RSA certificate and private key to the given paths. It will return any error encountered while creating the file,
// or encoding either the certificate or key to their respective formats.
func SaveRSA(certPath string, keyPath string, cert *x509.Certificate, priv *rsa.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Str("key_path", keyPath).Msg("Key.SaveRSA: Saving RSA key")
	err := SaveCert(certPath, cert)
	if err != nil {
		return err
	}
	return SaveRSAPrivateKey(keyPath, priv)
}

// SaveECDSA saves an ECDSA certificate and private key to the given paths. It will return any error encountered while creating the file,
// or encoding either the certificate or key to their respective formats.
func SaveECDSA(certPath string, keyPath string, cert *x509.Certificate, priv *ecdsa.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Str("key_path", keyPath).Msg("Key.SaveECDSA: Saving ECDSA key")
	err := SaveCert(certPath, cert)
	if err != nil {
		return err
	}
	return SaveECDSAPrivateKey(keyPath, priv)
}

// SaveEd25519 saves an Ed25519 certificate and private key to the given paths. It will return any error encountered while creating the file,
// or encoding either the certificate or key to their respective formats.
func SaveEd25519(certPath string, keyPath string, cert *x509.Certificate, priv *ed25519.PrivateKey) error {
	kttlog.Logger.Trace().Str("path", keyPath).Str("key_path", keyPath).Msg("Key.SaveEd25519: Saving Ed25519 key")
	err := SaveCert(certPath, cert)
	if err != nil {
		return err
	}
	return SaveEd25519PrivateKey(keyPath, priv)
}

// SaveKeyWithPath saves the given key's certificate and private key to the given paths. It will return
// any errors encountered saving the two values.
func (k *Key) SaveKeyWithPath(certPath string, keyPath string) error {
	kttlog.Logger.Trace().Str("path", keyPath).Str("key_path", keyPath).Strs("domains", k.domains).Msg("Key.SaveKeyWithPath: Saving Key")
	switch k.privAlg {
	case x509.RSA:
		return SaveRSA(certPath, keyPath, k.cert.Leaf, k.privRSA)
	case x509.ECDSA:
		return SaveECDSA(certPath, keyPath, k.cert.Leaf, k.privECDSA)
	case x509.Ed25519:
		return SaveEd25519(certPath, keyPath, k.cert.Leaf, k.privEd25519)
	}
	return &UnsupportedPairTypeError{k.privAlg.String()}
}

// SavePubKeyWithPath saves the given key's public key to the given path. It will return any error encountered
// while saving the key.
func (k *Key) SavePubKeyWithPath(path string) error {
	pub := k.GetPublicKeyBytes()
	return SavePEMBytes(path, netcommon.PEMHeaderPub, pub)
}

// SaveKey saves the Key to a randomly-generated path under the .keystore directory. It will return any error encountered while creating the file,
// or encoding the key's attributes to PEM format.
func (k *Key) SaveKey() error {
	kttlog.Logger.Info().Strs("domains", k.domains).Msg("Key.SaveKey: Saving key")

	if !k.hasPriv {
		return &NoPrivateKeyError{}
	}

	name := uuid.NewString()
	path := filepath.Join(localKeyStoreDir, name)
	return k.SaveKeyWithPath(path+".pem", path+".key")
}

// GetCertificate returns the *x509.Certificate associated with the key.
func (k *Key) GetCertificate() *x509.Certificate {
	kttlog.Logger.Trace().Strs("domains", k.domains).Msg("Key.GetCertificate: Getting certificate")
	return k.cert.Leaf
}

// GetTLSCertificate returns the *tls.Certificate associated with the key.
func (k *Key) GetTLSCertificate() *tls.Certificate {
	kttlog.Logger.Trace().Strs("domains", k.domains).Msg("Key.GetTLSCertificate: Getting TLS certificate")
	return k.cert
}
