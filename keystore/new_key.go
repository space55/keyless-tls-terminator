package keystore

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"io"
	"os"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

// NewKeyFromTLSCertificate creates a new *Key from a given *tls.Certificate
func NewKeyFromTLSCertificate(cert *tls.Certificate) *Key {
	kttlog.Logger.Trace().Str("cert_cn", cert.Leaf.Subject.CommonName).Msg("keystore.NewKeyFromTLSCertificate: Creating new Key from TLS certificate")
	domains := cert.Leaf.DNSNames
	k := &Key{
		domains: domains,
		cert:    cert,
		hasPriv: false,
	}

	return k
}

// NewKeyFromRSA creates a new *Key from a given *x509.Certificate and *rsa.PrivateKey
func NewKeyFromRSA(cert *x509.Certificate, priv *rsa.PrivateKey) *Key {
	kttlog.Logger.Trace().Str("cert_cn", cert.Subject.CommonName).Msg("keystore.NewKeyFromRSA: Creating new Key from RSA certificate")
	k := &Key{
		domains: cert.DNSNames,
		cert:    &tls.Certificate{Leaf: cert, PrivateKey: priv},
		hasPriv: true,
		privAlg: x509.RSA,
		pubRSA:  cert.PublicKey.(*rsa.PublicKey),
		privRSA: priv,
	}

	return k
}

// NewKeyFromECDSA creates a new *Key from a given *x509.Certificate and *ecdsa.PrivateKey
func NewKeyFromECDSA(cert *x509.Certificate, priv *ecdsa.PrivateKey) *Key {
	kttlog.Logger.Trace().Str("cert_cn", cert.Subject.CommonName).Msg("keystore.NewKeyFromECDSA: Creating new Key from ECDSA certificate")
	k := &Key{
		domains:   cert.DNSNames,
		cert:      &tls.Certificate{Leaf: cert, PrivateKey: priv},
		hasPriv:   true,
		privAlg:   x509.ECDSA,
		pubECDSA:  cert.PublicKey.(*ecdsa.PublicKey),
		privECDSA: priv,
	}

	return k
}

// NewKeyFromEd25519 creates a new *Key from a given *x509.Certificate and *ed25519.PrivateKey
func NewKeyFromEd25519(cert *x509.Certificate, priv *ed25519.PrivateKey) *Key {
	kttlog.Logger.Trace().Str("cert_cn", cert.Subject.CommonName).Msg("keystore.NewKeyFromEd25519: Creating new Key from Ed25519 certificate")
	pub := cert.PublicKey.(ed25519.PublicKey)
	k := &Key{
		domains:     cert.DNSNames,
		cert:        &tls.Certificate{Leaf: cert, PrivateKey: priv},
		hasPriv:     true,
		privAlg:     x509.Ed25519,
		pubEd25519:  &pub,
		privEd25519: priv,
	}

	return k
}

// NewKeyFromPKCS8Bytes creates a new *Key a given certificate and private key, in PKCS8 format
func NewKeyFromPKCS8Bytes(cert []byte, priv []byte) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromPKCS8Bytes: Creating new Key from PKCS8 bytes")
	privParsed, err := x509.ParsePKCS8PrivateKey(priv)
	if err != nil {
		return nil, err
	}

	certParsed, err := x509.ParseCertificate(cert)
	if err != nil {
		return nil, err
	}

	switch certParsed.PublicKeyAlgorithm {
	case x509.RSA:
		priv, ok := privParsed.(*rsa.PrivateKey)
		if !ok {
			return nil, &UnsupportedPairTypeError{certParsed.PublicKeyAlgorithm.String()}
		}
		return NewKeyFromRSA(certParsed, priv), nil
	case x509.ECDSA:
		priv, ok := privParsed.(*ecdsa.PrivateKey)
		if !ok {
			return nil, &UnsupportedPairTypeError{certParsed.PublicKeyAlgorithm.String()}
		}
		return NewKeyFromECDSA(certParsed, priv), nil
	case x509.Ed25519:
		priv, ok := privParsed.(*ed25519.PrivateKey)
		if !ok {
			return nil, &UnsupportedPairTypeError{certParsed.PublicKeyAlgorithm.String()}
		}
		return NewKeyFromEd25519(certParsed, priv), nil
	}

	return nil, &UnsupportedPairTypeError{certParsed.PublicKeyAlgorithm.String()}
}

// NewKeyFromRSABytes creates a new *Key a given certificate and private key, in PKCS1 format
func NewKeyFromRSABytes(cert []byte, priv []byte) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromRSABytes: Creating new Key from RSA bytes")
	privParsed, err := x509.ParsePKCS1PrivateKey(priv)
	if err != nil {
		return nil, err
	}

	certParsed, err := x509.ParseCertificate(cert)
	if err != nil {
		return nil, err
	}

	return NewKeyFromRSA(certParsed, privParsed), nil
}

// NewKeyFromPKCS8File creates a new *Key a given certificate and private key, in whatever format
// Golang uses for the x509.ParseECPrivateKey function.
func NewKeyFromECDSABytes(cert []byte, priv []byte) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromECDSABytes: Creating new Key from ECDSA bytes")
	privParsed, err := x509.ParseECPrivateKey(priv)
	if err != nil {
		return nil, err
	}

	certParsed, err := x509.ParseCertificate(cert)
	if err != nil {
		return nil, err
	}

	return NewKeyFromECDSA(certParsed, privParsed), nil
}

// NewKeyFromRSAFile creates a new *Key a given certificate and private key, in whatever format
// Golang uses for the ed25519.PrivateKey function. Though, it doesn't seem to actually have
// a "format", so to speak, it just stores bytes and then performs operations directly
// on those bytes.
func NewKeyFromEd25519Bytes(cert []byte, priv []byte) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromEd25519Bytes: Creating new Key from Ed25519 bytes")
	privParsed := ed25519.PrivateKey(priv)

	certParsed, err := x509.ParseCertificate(cert)
	if err != nil {
		return nil, err
	}

	return NewKeyFromEd25519(certParsed, &privParsed), nil
}

// NewKeyFromPem creates a new *Key from a given pair of PEM blocks. It returns any error encountered
// during processing, along with the *Key.
func NewKeyFromPem(cert *pem.Block, priv *pem.Block) (*Key, error) {
	kttlog.Logger.Trace().Str("cert_header", cert.Type).Str("key_header", priv.Type).Msg("keystore.NewKeyFromPem: Creating new Key from PEM")
	switch priv.Type {
	case netcommon.PEMHeaderDefaultPriv:
		return NewKeyFromPKCS8Bytes(cert.Bytes, priv.Bytes)
	case netcommon.PEMHeaderRSAPriv:
		return NewKeyFromRSABytes(cert.Bytes, priv.Bytes)
	case netcommon.PEMHeaderECDSAPriv:
		return NewKeyFromECDSABytes(cert.Bytes, priv.Bytes)
	case netcommon.PEMHeaderEd25519Priv:
		return NewKeyFromEd25519Bytes(cert.Bytes, priv.Bytes)
	}

	return nil, &UnsupportedPairTypeError{priv.Type}
}

// NewKeyFromBytes creates a new *Key from a given pair of PEM-encoded byte slices. It returns any error
// encountered during processing, along with the *Key.
func NewKeyFromBytes(cert []byte, priv []byte) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromBytes: Creating new Key from bytes")
	certPem, _ := pem.Decode(cert)
	privPem, _ := pem.Decode(priv)

	return NewKeyFromPem(certPem, privPem)
}

// NewKeyFromFile creates a new *Key from a given pair of PEM-encoded files, read through an io.Reader.
// It returns any error encountered during processing, along with the *Key.
func NewKeyFromReader(cert io.Reader, priv io.Reader) (*Key, error) {
	kttlog.Logger.Trace().Msg("keystore.NewKeyFromReader: Creating new Key from reader")
	certPem, err := netcommon.ReadPem(cert)
	if err != nil {
		return nil, err
	}
	privPem, err := netcommon.ReadPem(priv)
	if err != nil {
		return nil, err
	}

	return NewKeyFromPem(certPem, privPem)
}

// NewKeyFromFile creates a new *Key from a given pair of PEM-encoded files.
// It returns any error encountered during processing, along with the *Key.
func NewKeyFromFile(certFile string, privFile string) (*Key, error) {
	kttlog.Logger.Trace().Str("cert_path", certFile).Str("key_path", privFile).Msg("keystore.NewKeyFromFile: Creating new Key from file")
	certFileReader, err := os.Open(certFile)
	if err != nil {
		return nil, err
	}

	privFileReader, err := os.Open(privFile)
	if err != nil {
		return nil, err
	}

	return NewKeyFromReader(certFileReader, privFileReader)
}
