package keystore

import (
	"crypto/x509"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/tchap/go-patricia/v2/patricia"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func ensureKeystoreDir() error {
	kttlog.Logger.Debug().Msg("keystore.ensureKeystoreDir: Ensuring keystore directory")
	if _, err := os.Stat(localKeyStoreDir); os.IsNotExist(err) {
		return os.Mkdir(localKeyStoreDir, 0600)
	}
	return nil
}

func (k *KeyStore) newFPWalker(acceptor KeyAcceptor) func(string, fs.DirEntry, error) error {
	return func(path string, entry fs.DirEntry, err error) error {
		kttlog.Logger.Trace().Str("path", path).Msg("KeyStore.newFPWalker: Walking filepath")
		if err != nil {
			return err
		}

		if entry.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".pem" && filepath.Ext(path) != ".crt" {
			return nil
		}

		keyPath := path[:len(path)-4] + ".key"

		kttlog.Logger.Debug().Str("path", path).Msg("KeyStore.newFPWalker: Loading key")

		newKey, err := NewKeyFromFile(path, keyPath)
		if err != nil {
			kttlog.Logger.Error().Err(err).Str("path", path).Msg("KeyStore.newFPWalker: Failed to load key")
			return err
		}

		k.AddKey(newKey)

		return nil
	}
}

// ReadLocalKeys reads all keys from a given path, and adds them to the keystore if the acceptor function
// returns true.
func (k *KeyStore) ReadLocalKeys(path string, acceptor KeyAcceptor) error {
	kttlog.Logger.Info().Str("path", path).Msg("KeyStore.ReadLocalKeys: Reading local keys")
	return filepath.WalkDir(path, k.newFPWalker(acceptor))
}

// ReadAllLocalKeys reads all keys from the given path, and adds them to the keystore.
func (k *KeyStore) ReadAllLocalKeys(path string) error {
	acceptor := func(_ *x509.Certificate) bool { return true }
	return k.ReadLocalKeys(path, acceptor)
}

func (k *KeyStore) storeKeyVisitor(prefix patricia.Prefix, item patricia.Item) error {
	key := item.(*Key)
	kttlog.Logger.Trace().Str("prefix", string(prefix)).Msg("KeyStore.storeKeyVisitor: Storing key")

	err := key.SaveKey()
	switch err.(type) {
	case *NoPrivateKeyError:
		return nil
	default:
		return err
	}
}

// SaveLocalKeys saves all keys in the keystore to their respective files. Note that this
// saves keys to disk, which might be a security risk, depending on your deployment style.
func (k *KeyStore) SaveLocalKeys() error {
	kttlog.Logger.Info().Str("name", k.Name).Msg("KeyStore.SaveLocalKeys: Saving local keys")
	err := ensureKeystoreDir()
	if err != nil {
		return err
	}
	k.accessMutex.RLock()
	clone := k.trie.Clone()
	k.accessMutex.RUnlock()

	kttlog.Logger.Trace().Str("name", k.Name).Msg("KeyStore.SaveLocalKeys: Trie cloned")

	return clone.Visit(k.storeKeyVisitor)
}
