package keystore

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"sync"

	"github.com/tchap/go-patricia/v2/patricia"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

// NewKeyStore creates a new KeyStore instance with the given name.
// The name parameter is only used for debugging, so it can be any
// valid string. It returns a *KeyStore struct that can be used to
// store and retrieve Key structs.
func NewKeyStore(name string) *KeyStore {
	return &KeyStore{
		Name:        name,
		trie:        patricia.NewTrie(),
		accessMutex: &sync.RWMutex{},
	}
}

// AddCert adds a tls.Certificate to the KeyStore.
func (s *KeyStore) AddCert(cert *tls.Certificate) {
	kttlog.Logger.Info().Strs("domains", cert.Leaf.DNSNames).Msg("KeyStore.AddCert: Adding certificate to trie")
	key := NewKeyFromTLSCertificate(cert)

	s.AddKey(key)
}

// AddKey adds a Key to the KeyStore.
func (s *KeyStore) AddKey(key *Key) {
	kttlog.Logger.Info().Strs("domains", key.domains).Msg("KeyStore.AddKey: Adding key to trie")

	s.accessMutex.Lock()
	defer s.accessMutex.Unlock()

	for _, domain := range key.domains {
		kttlog.Logger.Trace().Str("domain", domain).Msg("KeyStore.AddKey: Adding key")
		s.trie.Insert(patricia.Prefix(domain), key)
	}
}

// DelCert deletes any entries in the KeyStore that would support the given x509.Certificate
func (s *KeyStore) DelCert(cert *x509.Certificate) {
	kttlog.Logger.Info().Strs("domains", cert.DNSNames).Msg("KeyStore.DelCert: Deleting certificate from trie")

	s.accessMutex.Lock()
	defer s.accessMutex.Unlock()
	kttlog.Logger.Trace().Strs("domains", cert.DNSNames).Msg("KeyStore.DelCert: Acquired lock")

	for _, domain := range cert.DNSNames {
		kttlog.Logger.Trace().Str("domain", domain).Msg("KeyStore.DelCert: Deleting key")
		deleted := s.trie.Delete(patricia.Prefix(domain))
		kttlog.Logger.Trace().Str("domain", domain).Bool("deleted", deleted).Msg("KeyStore.DelCert: Deleted key")
	}

	kttlog.Logger.Trace().Strs("domains", cert.DNSNames).Msg("KeyStore.DelCert: Deleted cert")
}

// GetCert returns the tls.Certificate for the given domain.
func (s *KeyStore) GetCert(domain string) *tls.Certificate {
	kttlog.Logger.Warn().Str("domain", domain).Msg("KeyStore.GetCert: Direct get for certificate is slow - use dedicated ops instead")

	key := s.GetKey(domain)
	if key == nil {
		return nil
	}

	return key.cert
}

// GetKey returns the Key for the given domain.
func (s *KeyStore) GetKey(domain string) *Key {
	kttlog.Logger.Trace().Str("domain", domain).Msg("KeyStore.GetKey: Getting key")
	s.accessMutex.RLock()
	defer s.accessMutex.RUnlock()
	key := s.lookup(domain)
	if key != nil {
		return key
	}

	if s.KeyRetriever == nil {
		return nil
	}

	key, err := s.KeyRetriever(domain)
	if err != nil {
		kttlog.Logger.Error().Err(err).Str("domain", domain).Msg("KeyStore.GetKey: Error retrieving key from remote KeyRetriever")
		return nil
	}

	return key
}

// Sign signs a challenge for a given domain, using the specified hash function and signature type. It will
// return the signature, or an error if the signing failed. Note that the data must be hashed *before* it is passed
// to this function. The hashFunc is simply to indicate the hashing algorithm used in the signature.
func (s *KeyStore) Sign(domain string, hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte) ([]byte, error) {
	s.accessMutex.RLock()
	defer s.accessMutex.RUnlock()
	key := s.GetKey(domain)
	if key == nil {
		return nil, &KeyNotFoundError{key: domain}
	}

	kttlog.Logger.Debug().Str("domain", domain).Int("len", len(challenge)).Str("hash_func", hashFunc.String()).Msg("KeyStore.Sign: Signing challenge")
	signed, err := key.Sign(hashFunc, sigType, challenge)
	if err != nil {
		return nil, err
	}

	if s.VerifySentSignatures {
		kttlog.Logger.Debug().Str("domain", domain).Msg("KeyStore.Sign: Verifying challenge")
		err = key.Verify(hashFunc, sigType, challenge, signed)
		if err != nil {
			kttlog.Logger.Error().Err(err).Str("domain", domain).Msg("KeyStore.Sign: Failed to verify challenge")
			return nil, err
		}
		kttlog.Logger.Debug().Str("domain", domain).Msg("KeyStore.Sign: Verified challenge")
	}

	return signed, nil
}
