package keystore_test

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/tls"
	"crypto/x509"
	"math/rand"
	"os"
	"os/exec"
	"testing"

	"github.com/rs/zerolog"
	"gitlab.com/space55/keyless-tls-terminator/keystore"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

func getRandomPrefix() string {
	b := make([]rune, 16)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func lookup(s *keystore.KeyStore, name string, shouldExist bool, t *testing.T) {
	ret := s.GetCert(name)
	if ret == nil && shouldExist {
		t.Fatal("Expected cert for " + name + " to exist")
	}
	if ret != nil && !shouldExist {
		t.Fatal("Expected cert for " + name + " to not exist")
	}
}

func genKey(s *keystore.KeyStore, certPool *x509.CertPool, certs *[]*tls.Certificate, domains *[]string, generator func([]string) (*keystore.Key, error), t *testing.T) {
	for i := 0; i < 1; i++ {
		domA := getRandomPrefix() + ".example.com"
		domB := getRandomPrefix() + ".example.com"
		*domains = append(*domains, domA, domB)
		key, err := generator([]string{domA, domB})
		if err != nil {
			t.Fatal(err)
		}
		s.AddKey(key)
		certPool.AddCert(key.GetCertificate())
		*certs = append(*certs, key.GetTLSCertificate())
	}
}

func TestKeyStore(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	s := keystore.NewKeyStore("test")

	certPool := x509.NewCertPool()

	rootKey, err := keystore.NewDummyKey([]string{"example.com"})
	if err != nil {
		t.Fatal(err)
	}
	s.AddKey(rootKey)
	certPool.AddCert(rootKey.GetCertificate())

	wildcardKey, err := keystore.NewDummyKey([]string{"*.example.com"})
	if err != nil {
		t.Fatal(err)
	}
	s.AddKey(wildcardKey)
	certPool.AddCert(wildcardKey.GetCertificate())

	domains := make([]string, 0, 10)
	certs := make([]*tls.Certificate, 0, 5)

	genKey(s, certPool, &certs, &domains, keystore.NewDummyKeyRSA, t)
	genKey(s, certPool, &certs, &domains, keystore.NewDummyKeyECDSA, t)
	genKey(s, certPool, &certs, &domains, keystore.NewDummyKeyEd25519, t)

	t.Log("Length of certs: ", len(certs))
	t.Log("Length of domains: ", len(domains))

	lookup(s, "example.com", true, t)
	lookup(s, "abcde.example.com", true, t)
	lookup(s, "abcde.abcde.example.com", false, t)
	for i := 2; i < len(domains); i++ {
		lookup(s, domains[i], true, t)
	}

	t.Log("Testing delete on ", domains[0])

	s.DelCert(certs[0].Leaf)
	t.Log("Testing for working wildcards")
	for i := 0; i < len(certs[0].Leaf.DNSNames); i++ {
		lookup(s, certs[0].Leaf.DNSNames[i], true, t)
	}
	t.Log("Testing the rest of the domains are still there")
	for i := 2; i < len(domains); i++ {
		lookup(s, domains[i], true, t)
	}

	t.Log("Testing that no subdomains exist on missing domain")
	lookup(s, "abcde."+domains[0], false, t)
	t.Log("Testing that no subdomains exist on present domain")
	lookup(s, "abcde."+domains[3], false, t)

	testData := []byte("test data")
	hashed := netcommon.ComputeHashSHA256(testData)
	t.Log("Test data: ", testData)
	t.Log("Signed data: ", hashed)

	signed, err := s.Sign("abcde.example.com", crypto.SHA256, netcommon.SigType_ECDSA, hashed)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Got signed: ", signed)

	// Only works because the wildcard cert is ECDSA
	signerCert := s.GetCert("abcde.example.com")
	t.Log("Checking signature with ", signerCert.Leaf.DNSNames)

	valid := ecdsa.VerifyASN1(signerCert.Leaf.PublicKey.(*ecdsa.PublicKey), hashed, signed)
	if !valid {
		t.Fatal("Signature is invalid")
	}

	// End ECDSA weirdness

	t.Log("Saving local keys")
	err = s.SaveLocalKeys()
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Reading local keys")

	s2 := keystore.NewKeyStore("test load")
	err = s2.ReadLocalKeys(".keystore", func(_ *x509.Certificate) bool {
		return true
	})
	if err != nil {
		t.Fatal(err)
	}

	lookup(s2, "example.com", true, t)
}

func saveData(path string, data []byte, t *testing.T) {
	f, err := os.Create(path)
	if err != nil {
		t.Fatal(err)
	}

	_, err = f.Write(data)
	if err != nil {
		t.Fatal(err)
	}
}

func TestVsOpenSSL(t *testing.T) {
	s := keystore.NewKeyStore("openssl-test")
	k, err := keystore.NewDummyKeyECDSA([]string{"example.com"})
	if err != nil {
		t.Fatal(err)
	}
	s.AddKey(k)

	clientRandom := make([]byte, 32)
	serverRandom := make([]byte, 32)
	curveInfo := []byte{0x03, 0x00, 0x1d}
	pubkeyData := k.GetPublicKeyBytes()
	err = k.SavePubKeyWithPath("/tmp/keystore-openssl-test-pub")
	if err != nil {
		t.Fatal(err)
	}
	err = k.SaveKeyWithPath("/tmp/keystore-openssl-test-cert", "/tmp/keystore-openssl-test-key")
	if err != nil {
		t.Fatal(err)
	}

	_, err = rand.Read(clientRandom)
	if err != nil {
		t.Fatal(err)
	}
	_, err = rand.Read(serverRandom)
	if err != nil {
		t.Fatal(err)
	}

	rawData := append(clientRandom, serverRandom...)
	rawData = append(rawData, curveInfo...)
	rawData = append(rawData, pubkeyData...)
	saveData("/tmp/keystore-openssl-test-raw", rawData, t)
	hashed := netcommon.ComputeHashSHA256(rawData)
	//hashed := rawData
	saveData("/tmp/keystore-openssl-test-hashed", hashed, t)

	signed, err := s.Sign("example.com", crypto.SHA256, netcommon.SigType_ECDSA, hashed)
	if err != nil {
		t.Fatal(err)
	}

	valid := ecdsa.VerifyASN1(k.GetPublicKeyECDSA(), hashed, signed)
	if !valid {
		t.Fatal("Signature is invalid")
	}
	t.Log("Signature valid")

	saveData("/tmp/keystore-openssl-test-signed", signed, t)

	cert := k.GetCertificate()
	err = netcommon.SaveCert("/tmp/keystore-openssl-test-cert", cert)
	if err != nil {
		t.Fatal(err)
	}

	out, err := exec.Command("openssl", "dgst", "-sha256", "-signature", "/tmp/keystore-openssl-test-signed", "-verify", "/tmp/keystore-openssl-test-pub", "/tmp/keystore-openssl-test-raw").CombinedOutput()
	t.Log("Output: ", string(out))
	if err != nil {
		t.Fatal(err)
	}

	t.Log("Output: ", out)
}
