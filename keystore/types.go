// keystore is a package which provides the necessary structs and functions to store and manage
// key pairs. It is used by the NKS to store, manage, sign, and verify keys.
package keystore

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"sync"

	"github.com/tchap/go-patricia/v2/patricia"
)

const localKeyStoreDir = ".keystore"

// Key is the type which contains all the information about a key. It contains the certificate,
// algorithm, the public key, and the private key.
type Key struct {
	hasPriv     bool
	privAlg     x509.PublicKeyAlgorithm
	domains     []string
	cert        *tls.Certificate
	pubRSA      *rsa.PublicKey
	privRSA     *rsa.PrivateKey
	pubECDSA    *ecdsa.PublicKey
	privECDSA   *ecdsa.PrivateKey
	pubEd25519  *ed25519.PublicKey
	privEd25519 *ed25519.PrivateKey
}

// KeyStore is a generic keystore which can store and manage keys. It is used by the NKS to store, manage, sign, and verify keys.
type KeyStore struct {
	// Name is the name of the KeyStore. It is only used for debugging, and is not necessary to be meaningful.
	Name        string
	trie        *patricia.Trie
	accessMutex *sync.RWMutex
	// KeyRetriever is a function which can be used to retrieve a key for a given domain from an external data source, which is not
	// implemented using the datastore.Datastore package.
	KeyRetriever         KeyRetriever
	VerifySentSignatures bool
}

// KeyAcceptor is a function which will return whether or not the given key should be loaded into the KeyStore based on its
// certificate. The private key is *not* loaded into memory unless this returns true.
type KeyAcceptor func(k *x509.Certificate) bool

// KeyRetriever returns a *Key for a given domain, or an error if there are any issues retrieving the key.
type KeyRetriever func(domain string) (*Key, error)

// KeyNotFoundError is an error which is returned when a key is not found in the KeyStore.
type KeyNotFoundError struct {
	key string
}

// UnsupportedPairTypeError is an error which is returned if a key pair specified is not supported by the
// keystore package.
type UnsupportedPairTypeError struct {
	certType string
}

// HashFuncNotSupportedError is an error which is returned if a hash function is not supported by the keystore package.
type HashFuncNotSupporedError struct {
	hashFunc string
}

// NoPrivateKeyError is an error which is returned if if a private key is not loaded into the key store, and
// an operation was requested which requires it.
type NoPrivateKeyError struct{}

// Error returns the error message for the KeyNotFoundError.
func (e *KeyNotFoundError) Error() string {
	return "key not found: " + e.key
}

// Error returns the error message for the UnsupportedPairTypeError.
func (e *UnsupportedPairTypeError) Error() string {
	return "unsupported pair type: " + e.certType
}

// Error returns the error message for the NoPrivateKeyError.
func (e *NoPrivateKeyError) Error() string {
	return "no private key found"
}

// Error returns the error message for the HashFuncNotSupporedError.
func (e *HashFuncNotSupporedError) Error() string {
	return "hash function not supported: " + e.hashFunc
}
