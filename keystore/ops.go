package keystore

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"strings"

	"github.com/tchap/go-patricia/v2/patricia"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

func (s *KeyStore) debugItem(prefix patricia.Prefix, item patricia.Item) error {
	key := item.(*Key)
	kttlog.Logger.Info().Str("name", s.Name).Str("prefix", string(prefix)).Strs("domains", key.domains).Msg("KeyStore.debugItem: Key found")

	return nil
}

func (s *KeyStore) debugOutput() {
	copy := s.trie.Clone()
	err := copy.Visit(s.debugItem)
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("KeyStore.debugOutput: Error while visiting trie")
	}
}

func (s *KeyStore) lookup(domain string) *Key {
	kttlog.Logger.Trace().Str("name", s.Name).Str("domain", domain).Msg("KeyStore.lookup: Looking up key for domain")
	item := s.trie.Get(patricia.Prefix(domain))
	if item == nil {
		if !strings.HasPrefix(domain, "*.") {
			issuer := netcommon.GetWildcardIssuer(domain)
			if len(issuer) == 0 {
				return nil
			}
			return s.lookup(issuer)
		}
		return nil
	}

	key := item.(*Key)
	return key
}

func isValidSignatureRSA(key *rsa.PublicKey, data []byte, sig []byte) bool {
	return rsa.VerifyPKCS1v15(key, crypto.SHA256, data, sig) == nil
}

func isValidSignatureECDSA(key *ecdsa.PublicKey, data []byte, sig []byte) bool {
	return ecdsa.VerifyASN1(key, data, sig)
}

// Sign is the function that computes a signature for a given challenge on the Key. Note that
// the challenge *must* be hashed before calling this function, as the hashFunc is only used
// to indicate the hash function parameter in the signature. It will return the signed data,
// or any error encountered.
func (k *Key) Sign(hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte) ([]byte, error) {
	kttlog.Logger.Debug().Strs("domains", k.domains).Str("hash_func", hashFunc.String()).Str("sig_type", sigType.String()).Msg("Key.Sign: Signing data")
	if !k.hasPriv {
		return nil, &NoPrivateKeyError{}
	}

	switch k.privAlg {
	case x509.RSA:
		switch sigType {
		case netcommon.SigType_RSA_PKCS1:
			return rsa.SignPKCS1v15(rand.Reader, k.privRSA, hashFunc, challenge)
		case netcommon.SigType_RSA_PSS:
			return rsa.SignPSS(rand.Reader, k.privRSA, hashFunc, challenge, &rsa.PSSOptions{SaltLength: rsa.PSSSaltLengthEqualsHash})
		default:
			return nil, &netcommon.UnknownSigTypeError{SigType: sigType}
		}
	case x509.ECDSA:
		return ecdsa.SignASN1(rand.Reader, k.privECDSA, challenge)
	case x509.Ed25519:
		return ed25519.Sign(*k.privEd25519, challenge), nil
	default:
		return nil, x509.ErrUnsupportedAlgorithm
	}
}

// Verify verifies that a provided signature is valid for a given challenge. Note that the challenge *must*
// be hashed before being provided. It will return any error encountered verifying the signature, or nil if
// the signature is valid.
func (k *Key) Verify(hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte, signed []byte) error {
	kttlog.Logger.Debug().Strs("domains", k.domains).Str("hash_func", hashFunc.String()).Str("sig_type", sigType.String()).Msg("Key.Verify: Verifying data")
	switch k.privAlg {
	case x509.RSA:
		switch sigType {
		case netcommon.SigType_RSA_PKCS1:
			return rsa.VerifyPKCS1v15(k.pubRSA, hashFunc, challenge, signed)
		case netcommon.SigType_RSA_PSS:
			return rsa.VerifyPSS(k.pubRSA, hashFunc, challenge, signed, &rsa.PSSOptions{SaltLength: rsa.PSSSaltLengthEqualsHash})
		default:
			return &netcommon.UnknownSigTypeError{SigType: sigType}
		}
	case x509.ECDSA:
		valid := ecdsa.VerifyASN1(k.pubECDSA, challenge, signed)
		if !valid {
			return &netcommon.InvalidSignatureError{}
		}
		return nil
	case x509.Ed25519:
		valid := ed25519.Verify(*k.pubEd25519, challenge, signed)
		if !valid {
			return &netcommon.InvalidSignatureError{}
		}
		return nil
	default:
		return x509.ErrUnsupportedAlgorithm
	}
}

// GetPublicKeyPEM gets the public key in PEM format.
func (k *Key) GetPublicKeyPEM() []byte {
	pub := k.GetPublicKeyBytes()
	block := pem.Block{Type: netcommon.PEMHeaderPub, Bytes: pub}
	return pem.EncodeToMemory(&block)
}

// GetPublicKeyBytes gets the public key in bytes, marshelled according to the x509.MarshalPKIXPublicKey function.
func (k *Key) GetPublicKeyBytes() []byte {
	data, err := x509.MarshalPKIXPublicKey(k.GetPublicKey())
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("Key.GetPublicKeyBytes: Failed to marshal public key")
	}
	return data
}

// GetPublicKey returns the raw crypto.PublicKey that is stored in this Key.
func (k *Key) GetPublicKey() crypto.PublicKey {
	switch k.privAlg {
	case x509.RSA:
		return crypto.PublicKey(k.pubRSA)
	case x509.ECDSA:
		return crypto.PublicKey(k.pubECDSA)
	case x509.Ed25519:
		return crypto.PublicKey(*k.pubEd25519)
	}

	return nil
}

// GetPublicKeyRSA returns the raw *rsa.PublicKey that is stored in this Key, if the algorithm used is RSA. Otherwise, it returns nil
func (k *Key) GetPublicKeyRSA() *rsa.PublicKey {
	if k.privAlg == x509.RSA {
		return k.pubRSA
	}
	return nil
}

// GetPublicKeyECDSA returns the raw *ecdsa.PublicKey that is stored in this Key, if the algorithm used is ECDSA. Otherwise, it returns nil
func (k *Key) GetPublicKeyECDSA() *ecdsa.PublicKey {
	if k.privAlg == x509.ECDSA {
		return k.pubECDSA
	}
	return nil
}

// GetPublicKeyEd25519 returns the raw *ed25519.PublicKey that is stored in this Key, if the algorithm used is Ed25519. Otherwise, it returns nil
func (k *Key) GetPublicKeyEd25519() *ed25519.PublicKey {
	if k.privAlg == x509.Ed25519 {
		return k.pubEd25519
	}
	return nil
}

// GetAlgorithm returns the x509.PublicKeyAlgorithm of the private key stored in this struct.
func (k *Key) GetAlgorithm() x509.PublicKeyAlgorithm {
	switch k.privAlg {
	case x509.RSA:
		return x509.RSA
	case x509.ECDSA:
		return x509.ECDSA
	case x509.Ed25519:
		return x509.Ed25519
	}

	return x509.UnknownPublicKeyAlgorithm
}

// GetPrivateKey returns the raw crypto.PrivateKey that is stored by this struct.
func (k *Key) GetPrivateKey() crypto.PrivateKey {
	switch k.privAlg {
	case x509.RSA:
		return crypto.PrivateKey(*k.privRSA)
	case x509.ECDSA:
		return crypto.PrivateKey(*k.privECDSA)
	case x509.Ed25519:
		return crypto.PrivateKey(k.privEd25519)
	}

	return nil
}

// GetPrivateKeyRSA returns the raw *rsa.PrivateKey that is stored by this struct, if the algorithm used is RSA. Otherwise, it returns nil
func (k *Key) GetPrivateKeyRSA() *rsa.PrivateKey {
	if k.privAlg == x509.RSA {
		return k.privRSA
	}
	return nil
}

// GetPrivateKeyECDSA returns the raw *ecdsa.PrivateKey that is stored by this struct, if the algorithm used is ECDSA. Otherwise, it returns nil
func (k *Key) GetPrivateKeyECDSA() *ecdsa.PrivateKey {
	if k.privAlg == x509.ECDSA {
		return k.privECDSA
	}
	return nil
}

// GetPrivateKeyEd25519 returns the raw *ed25519.PrivateKey that is stored by this struct, if the algorithm used is Ed25519. Otherwise, it returns nil
func (k *Key) GetPrivateKeyEd25519() *ed25519.PrivateKey {
	if k.privAlg == x509.Ed25519 {
		return k.privEd25519
	}
	return nil
}
