package keystore

import "gitlab.com/space55/keyless-tls-terminator/netcommon"

// NewDummyKey creates a new *Key from a dummy self-signed certificate and private key for
// the given domains. The domains are specified as SAN entries in the certificate.
func NewDummyKey(domains []string) (*Key, error) {
	return NewDummyKeyECDSA(domains)
}

// NewDummyKeyRSA creates a new *Key from a dummy self-signed certificate and RSA private key for
// the given domains. The domains are specified as SAN entries in the certificate.
func NewDummyKeyRSA(domains []string) (*Key, error) {
	cert, priv, err := netcommon.GenerateDummyPairRSA(domains)
	if err != nil {
		return nil, err
	}

	return NewKeyFromRSA(cert.Leaf, priv), nil
}

// NewDummyKeyECDSA creates a new *Key from a dummy self-signed certificate and ECDSA private key for
// the given domains. The domains are specified as SAN entries in the certificate.
func NewDummyKeyECDSA(domains []string) (*Key, error) {
	cert, priv, err := netcommon.GenerateDummyPairECDSA(domains)
	if err != nil {
		return nil, err
	}

	return NewKeyFromECDSA(cert.Leaf, priv), nil
}

// NewDummyKeyEd25519 creates a new *Key from a dummy self-signed certificate and Ed25519 private key for
// the given domains. The domains are specified as SAN entries in the certificate.
func NewDummyKeyEd25519(domains []string) (*Key, error) {
	cert, priv, err := netcommon.GenerateDummyPairEd25519(domains)
	if err != nil {
		return nil, err
	}

	return NewKeyFromEd25519(cert.Leaf, priv), nil
}
