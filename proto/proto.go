package proto

import (
	"crypto/rand"
	"encoding/binary"
)

func (m *Message) GetID() MessageID {
	return m.messageID
}

func (op *Op) GetContent() []byte {
	out := make([]byte, len(op.data))
	copy(out, op.data)
	return out
}

func (op *Op) GetType() OpType {
	return op.opType
}

func (op *Op) GetID() OpID {
	return op.opID
}

func genMessageID() MessageID {
	return MessageID(gen32BitID())
}

func genOpID() OpID {
	return OpID(gen32BitID())
}

func gen32BitID() uint32 {
	idBuf := make([]byte, 4)
	_, err := rand.Read(idBuf)

	if err != nil {
		return 0
	}

	return binary.BigEndian.Uint32(idBuf)
}
