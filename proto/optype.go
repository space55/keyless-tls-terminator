package proto

import (
	"crypto"

	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

// Highest bit indicates if it is a reply
const OpType_DEBUG = 0x00
const OpType_PROOFREQ = 0x02
const OpType_PROOFREP = 0x82

type OpData interface {
	Marshal() ([]byte, error)
}

type ReplyingOp interface {
	GetReplyingTo() OpID
}

type Op_Debug struct {
	Content string `msgpack:"c"`
	OpData
}

type Op_ProofReq struct {
	Domain    string            `msgpack:"d"`
	Challenge []byte            `msgpack:"p"`
	HashFunc  crypto.Hash       `msgpack:"h"`
	SigType   netcommon.SigType `msgpack:"s"`
}

type Op_ProofRep struct {
	Proof        []byte `msgpack:"p"`
	ReplyingToID OpID   `msgpack:"r"`
	ReplyingOp
}

func IsReply(opType OpType) bool {
	return opType&0x80 == 0x80
}

func Unmarshal(optype OpType, data []byte) (OpData, error) {
	switch OpType(optype) {
	case OpType_DEBUG:
		ret, err := Unmarshal_Debug(data)
		return ret, err
	case OpType_PROOFREQ:
		return Unmarshal_ProofReq(data)
	case OpType_PROOFREP:
		return Unmarshal_ProofRep(data)
	}
	return nil, &unknownOpTypeErr{optype: optype}
}

func (o Op_ProofRep) GetReplyingTo() OpID {
	return o.ReplyingToID
}

func Unmarshal_Debug(data []byte) (*Op_Debug, error) {
	return &Op_Debug{Content: string(data)}, nil
}

func (o Op_Debug) Marshal() ([]byte, error) {
	return []byte(o.Content), nil
}

func Unmarshal_ProofReq(data []byte) (*Op_ProofReq, error) {
	kttlog.Logger.Trace().Int("data_len", len(data)).Msg("proto.Unmarshal_ProofReq: Unmarshaling data")
	op := &Op_ProofReq{}
	err := msgpack.Unmarshal(data, &op)
	if err != nil {
		return nil, err
	}

	return op, nil
}

func (o *Op_ProofReq) Marshal() ([]byte, error) {
	return msgpack.Marshal(o)
}

func Unmarshal_ProofRep(data []byte) (*Op_ProofRep, error) {
	o := &Op_ProofRep{}
	err := msgpack.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	return o, nil
}

func (r Op_ProofRep) Marshal() ([]byte, error) {
	return msgpack.Marshal(r)
}
