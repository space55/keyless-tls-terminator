package proto

import (
	"crypto/tls"
	"sync"
)

const msgHeaderSize = 8                          // Size of the message header in bytes
const maxMessageSize = 1024                      // 1KB, keep it low to allow data to be transmitted in a single TCP packet
const maxOpSize = maxMessageSize - msgHeaderSize // Maximum message size - header length
const opHeaderSize = 8                           // Size of the op header in bytes

const defaultMajorVersion = 1
const defaultMinorVersion = 0

type OpType uint8
type Length uint16
type MessageID uint32
type OpID uint32
type MajorVersion uint8
type MinorVersion uint8

// Message represents a message sent between two peers. It can contain multiple Op's
// Wire Format:
// 0 - Major Version
// 1 - Minor Version
// 2:4 - Message Length, computed by len(m.data)
// 4:8 - Message ID
// 8: - Message Data
type Message struct {
	data         []byte
	majorVersion MajorVersion
	minorVersion MinorVersion
	messageID    MessageID
	ops          []Op
	writeMutex   *sync.Mutex
}

// Op represents the op of a single operation. There can be multiple within a single Message
// Wire Format:
// 0 - Message Type
// 1 - Reserved
// 2:4 - Op Length, computed by len(b.data)
// 4:8 - Op ID
// 8: - Op Data
type Op struct {
	opType     OpType
	data       []byte
	writeMutex *sync.Mutex
	opID       OpID
}

type ProtoServer struct {
	Network string
	Laddr   string
	conn    *tls.Conn
}
