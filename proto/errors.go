package proto

import "strconv"

type msgInternalError struct {
	msg string
}

func (e *msgInternalError) Error() string {
	return e.msg
}

type msgTooLargeErr struct{}

func (e *msgTooLargeErr) Error() string {
	return "message too large"
}

type msgLengthOutOfBoundsErr struct{}

func (e *msgLengthOutOfBoundsErr) Error() string {
	return "message length out of bounds"
}

type msgInvalidErr struct{}

func (e *msgInvalidErr) Error() string {
	return "message invalid"
}

type opTooLargeErr struct{}

func (e *opTooLargeErr) Error() string {
	return "op too large"
}

type opLengthOutOfBoundsErr struct{}

func (e *opLengthOutOfBoundsErr) Error() string {
	return "op length out of bounds"
}

type opInvalidErr struct{}

func (e *opInvalidErr) Error() string {
	return "op invalid"
}

type unknownOpTypeErr struct {
	optype OpType
}

func (e *unknownOpTypeErr) Error() string {
	return "unknown op type: " + strconv.Itoa(int(e.optype))
}

type dangerLogDisabledErr struct{}

func (e *dangerLogDisabledErr) Error() string {
	return "dangerous logging disabled"
}
