package proto

import (
	"encoding/binary"
	"io"
	"strconv"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func NewMessage() *Message {
	m := &Message{
		writeMutex: &sync.Mutex{},
	}

	m.majorVersion = defaultMajorVersion
	m.minorVersion = defaultMinorVersion
	m.messageID = genMessageID()

	kttlog.Logger.Trace().Uint32("msg_id", uint32(m.messageID)).Msg("NewMessage: Allocated new message")

	return m
}

func newEmptyMessage() *Message {
	kttlog.Logger.Trace().Msg("newEmptyMessage: Allocating new empty message")
	return &Message{
		writeMutex: &sync.Mutex{},
	}
}

func ReadFrom(in io.Reader) (*Message, error) {
	buf := make([]byte, msgHeaderSize)
	n, err := io.ReadFull(in, buf)
	if err != nil {
		return nil, err
	}
	if n != msgHeaderSize {
		return nil, &msgInternalError{"n was not equal to msgHeaderSize"}
	}

	m := newEmptyMessage()

	m.majorVersion = MajorVersion(buf[0])
	m.minorVersion = MinorVersion(buf[1])
	msgLen := binary.BigEndian.Uint16(buf[2:4])
	m.messageID = MessageID(binary.BigEndian.Uint32(buf[4:8]))

	kttlog.Logger.Trace().Uint32("msg_id", uint32(m.messageID)).Int("msg_len", int(msgLen)).Msg("Message.ReadFrom: Header parsed")

	buf = make([]byte, msgLen)
	n, err = io.ReadFull(in, buf)
	if err != nil && err != io.EOF {
		return nil, err
	}
	if n != int(msgLen) {
		if err == io.EOF {
			return nil, io.ErrUnexpectedEOF
		}
		return nil, &msgInternalError{"n was not equal to msgHeaderSize"}
	}

	m.data = buf

	kttlog.Logger.Debug().Int("len", msgHeaderSize+n).Msg("Message.ReadFrom: Data parsed")

	return m, m.processOp()
}

func (m *Message) WriteTo(out io.Writer) (int64, error) {
	m.writeMutex.Lock()
	defer m.writeMutex.Unlock()

	buf := make([]byte, msgHeaderSize+len(m.data))
	buf[0] = uint8(m.majorVersion)
	buf[1] = uint8(m.minorVersion)
	binary.BigEndian.PutUint16(buf[2:4], uint16(len(m.data)))
	binary.BigEndian.PutUint32(buf[4:8], uint32(m.messageID))
	copy(buf[msgHeaderSize:], m.data)

	n, err := out.Write(buf)
	if err != nil {
		return int64(n), err
	}

	if n != msgHeaderSize+len(m.data) {
		return int64(n), &msgInternalError{"n was not equal to " + strconv.Itoa(msgHeaderSize+len(m.data))}
	}

	kttlog.Logger.Debug().Int("len", n).Msg("Message.WriteTo: Data written")

	return int64(n), err
}

func (m *Message) Push(b *Op) error {
	m.writeMutex.Lock()
	defer m.writeMutex.Unlock()

	m.ops = append(m.ops, *b)

	data := b.serialize()

	m.data = append(m.data, data...)

	kttlog.Logger.Trace().Int("new_len", len(m.data)).Msg("Message.Push: Appended data to slice")

	return nil
}

func (m *Message) PushMany(o []*Op) error {
	for _, oo := range o {
		if err := m.Push(oo); err != nil {
			return err
		}
	}

	return nil
}

func (m *Message) Pop() *Op {
	m.writeMutex.Lock()
	defer m.writeMutex.Unlock()

	if len(m.ops) == 0 {
		return nil
	}

	popInd := len(m.ops) - 1
	op := m.ops[popInd]
	m.ops = m.ops[:popInd]

	s := op.size()

	m.data = m.data[:len(m.data)-int(s)]

	kttlog.Logger.Trace().Int("new_len", len(m.data)).Msg("Message.Pop: Popped data from slice")

	return &op
}

func (m *Message) GetOps() []Op {
	m.writeMutex.Lock()
	defer m.writeMutex.Unlock()
	ops := make([]Op, 0, len(m.ops))
	ops = append(ops, m.ops...)

	return ops
}

func (m *Message) size() uint16 {
	m.writeMutex.Lock()
	defer m.writeMutex.Unlock()
	return uint16(msgHeaderSize + len(m.data))
}
