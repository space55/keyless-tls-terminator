package proto

import (
	"bytes"
	"encoding/binary"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func NewOp(opType OpType, data []byte) (*Op, error) {
	id := genOpID()
	kttlog.Logger.Debug().Uint32("op_id", uint32(id)).Uint8("op_type", uint8(opType)).Msg("NewOp: Generating new op")
	return newOpInternal(opType, id, data)
}

func newOpInternal(opType OpType, opID OpID, data []byte) (*Op, error) {
	if len(data) > maxOpSize {
		return nil, &opTooLargeErr{}
	}

	b := &Op{
		opType:     opType,
		data:       data,
		opID:       opID,
		writeMutex: &sync.Mutex{},
	}

	kttlog.Logger.Trace().Int("len", len(data)).Uint32("op_id", uint32(b.GetID())).Msg("proto.newOpInternal: New op created")

	return b, nil
}

func (b *Op) size() uint16 {
	s := uint16(opHeaderSize + len(b.data))
	kttlog.Logger.Trace().Uint16("size", s).Uint32("op_id", uint32(b.GetID())).Msg("Op.size: Calculated size")
	return s
}

func (m *Message) processOp() error {
	kttlog.Logger.Debug().Int("msg_len", len(m.data)).Msg("Message.processOp: Processing message")
	buf := bytes.NewBuffer(m.data)
	for buf.Len() > 0 {
		if buf.Len() < opHeaderSize {
			return &opInvalidErr{}
		}

		opTypeB, err := buf.ReadByte()
		if err != nil {
			return err
		}

		opType := OpType(opTypeB)

		_, err = buf.ReadByte() // Reserved
		if err != nil {
			return err
		}

		opLenBuf := make([]byte, 2)
		_, err = buf.Read(opLenBuf)
		if err != nil {
			return err
		}

		opLen := Length(binary.BigEndian.Uint16(opLenBuf))

		opIDBuf := make([]byte, 4)
		_, err = buf.Read(opIDBuf)
		if err != nil {
			return err
		}
		opID := OpID(binary.BigEndian.Uint32(opIDBuf))

		data := make([]byte, opLen)
		_, err = buf.Read(data)
		if err != nil {
			return &opInvalidErr{}
		}

		kttlog.Logger.Trace().Uint8("op_type", uint8(opType)).Uint32("op_id", uint32(opID)).Uint16("op_len", uint16(opLen)).Msg("Message.processOp: Processing op")

		op, err := newOpInternal(opType, opID, data)
		if err != nil {
			return err
		}
		err = m.Push(op)
		if err != nil {
			return err
		}
	}

	return nil
}

func (o *Op) serialize() []byte {
	s := o.size()

	buf := make([]byte, s)
	buf[0] = uint8(o.opType)
	buf[1] = 0 // Reserved
	binary.BigEndian.PutUint16(buf[2:4], uint16(len(o.data)))
	binary.BigEndian.PutUint32(buf[4:8], uint32(o.opID))
	copy(buf[opHeaderSize:], o.data)

	return buf
}

func (o *Op) Unmarshal() (OpData, error) {
	opdata, err := Unmarshal(o.opType, o.data)
	return opdata, err
}
