// kttlog is the package specifying logging information used throughout the ecosystem. It contains
// helper functions to make management of logging easier.
package kttlog

import (
	"io"
	"log/syslog"
	"os"
	"strings"

	"github.com/rs/zerolog"
)

var Logger *zerolog.Logger

var writers []io.Writer

var hooks []zerolog.Hook

func init() {
	ResetLogger()
	EnableConsoleLog()
}

// SetKTTLogger sets the logger to the given zerolog.Logger struct
func SetKTTLogger(l zerolog.Logger) {
	Logger = &l
}

// SetLevel sets the log level to a given zerolog.Level
func SetLevel(level zerolog.Level) {
	SetKTTLogger(Logger.Level(level))
}

// AddWriter adds an io.Writer to the list of writers, then rebuilds the logger
func AddWriter(w io.Writer) {
	writers = append(writers, w)
	RebuildLogger()
}

// ResetLogger resets the logger to the default zerolog.Logger, removing all writers and hooks
func ResetLogger() {
	writers = make([]io.Writer, 0)
	hooks = make([]zerolog.Hook, 0)
	SetKTTLogger(zerolog.New(io.Discard))
}

// RebuildLogger rebuilds the logger with the current list of writers and hooks
func RebuildLogger() {
	multi := zerolog.MultiLevelWriter(writers...)
	l := zerolog.New(multi).With().Timestamp().Logger()
	for _, h := range hooks {
		l.Hook(h)
	}
	SetKTTLogger(l)
}

// AddHook adds a hook to the list of hooks, then rebuilds the logger
func AddHook(hook zerolog.Hook) {
	hooks = append(hooks, hook)
	RebuildLogger()
}

// EnableConsoleLog enables the console logger (this is enabled by default, but is disabled if ResetLogger is called)
func EnableConsoleLog() {
	cw := zerolog.NewConsoleWriter()

	AddWriter(cw)

	Logger.Info().Msg("Console Log Enabled")
}

// EnableFileLog enables the file logger and sets the log file to the given path
func EnableFileLog(path string) error {
	w, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}

	AddWriter(w)

	Logger.Info().Str("path", path).Msg("File Log Enabled")

	return nil
}

// EnableNetworkSyslog enables the network syslog logger and sets the syslog server to the given host:port
func EnableNetworkSyslog(network, addr string) error {
	writer, err := syslog.Dial(network, addr, syslog.LOG_ALERT, "ktt")
	if err != nil {
		return err
	}

	w := zerolog.SyslogWriter(writer)
	sw := zerolog.SyslogLevelWriter(w)
	AddWriter(sw)

	Logger.Info().Msg("Networked Syslog Enabled")

	return nil
}

// EnableLocalSyslog enables the local syslog logger, according to the native syslog package
func EnableLocalSyslog() error {
	writer, err := syslog.Dial("", "", syslog.LOG_ALERT, "ktt")
	if err != nil {
		return err
	}

	w := zerolog.SyslogWriter(writer)
	sw := zerolog.SyslogLevelWriter(w)
	AddWriter(sw)

	Logger.Info().Msg("Local Syslog Enabled")

	return nil
}

// SetLevelStr sets the log level to a given string
func SetLevelStr(level string) {
	level = strings.ToLower(level)
	switch level {
	case "trace":
		SetLevel(zerolog.TraceLevel)
	case "debug":
		SetLevel(zerolog.DebugLevel)
	case "info":
		SetLevel(zerolog.InfoLevel)
	case "warn":
		SetLevel(zerolog.WarnLevel)
	case "error":
		SetLevel(zerolog.ErrorLevel)
	case "fatal":
		SetLevel(zerolog.FatalLevel)
	case "panic":
		SetLevel(zerolog.PanicLevel)
	case "none":
		SetLevel(zerolog.NoLevel)
	case "disabled":
		SetLevel(zerolog.Disabled)
	default:
		Logger.Warn().Str("desired_level", level).Msg("Unknown log level")
	}
	Logger.Info().Str("desired_level", level).Msg("Log level set")
}
