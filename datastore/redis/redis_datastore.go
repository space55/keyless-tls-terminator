// redis_datastore is a package that provides a datastore.Datastore implementation,
// connecting to Redis
package redis_datastore

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/space55/keyless-tls-terminator/datastore"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// RedisDatastore is the datastore implementation for Redis. It implements the datastore.Datastore interface,
// and can be provided to the NKS to store and retrieve private keys and certificates.
type RedisDatastore struct {
	redisAddress  string
	redisNetwork  string
	redisPassword string
	redisDB       int
	client        *redis.Client
	datastore.Datastore
}

var ctx = context.Background()

// NewRedisDatastore creates a new redis datastore for a given set of parameters. It returns a *RedisDatastore,
// which can be used by the NKS to store and retrieve private keys and certificates.
func NewRedisDatastore(address string, network string, password string, dbID int) (*RedisDatastore, error) {
	kttlog.Logger.Debug().Str("addr", address).Msg("redis_datastore.NewRedisDatastore: Creating new RedisDatastore")
	d := &RedisDatastore{
		redisAddress:  address,
		redisNetwork:  network,
		redisPassword: password,
		redisDB:       dbID,
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     d.redisAddress,
		Password: d.redisPassword, // no password set
		DB:       d.redisDB,       // use default DB
	})
	kttlog.Logger.Trace().Str("addr", d.redisAddress).Msg("redis_datastore.NewRedisDatastore: Datastore created")

	err := rdb.Ping(ctx).Err()
	if err != nil {
		return nil, err
	}

	d.client = rdb

	return d, nil
}

func getPrivDBKey(domain string) string {
	return fmt.Sprintf("%s:privkey", domain)
}

func getCertDBKey(domain string) string {
	return fmt.Sprintf("%s:cert", domain)
}

// Close terminates the connection to Redis
func (d *RedisDatastore) Close() error {
	return d.client.Close()
}

// GetPrivateKey retrieves the private key for a given domain from redis.
func (d *RedisDatastore) GetPrivateKey(domain string) ([]byte, error) {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.GetPrivateKey: Getting private key")
	key := getPrivDBKey(domain)
	cmd := d.client.Get(ctx, key)

	return cmd.Bytes()
}

// GetPrivateKey retrieves the certificate for a given domain from redis.
func (d *RedisDatastore) GetCertificate(domain string) ([]byte, error) {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.GetCertificate: Getting certificate")
	key := getCertDBKey(domain)
	cmd := d.client.Get(ctx, key)

	return cmd.Bytes()
}

// GetPrivateKey stores the private key for a given domain in redis.
func (d *RedisDatastore) SavePrivateKey(domain string, keyBytes []byte) error {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.SavePrivateKey: Saving private key")
	key := getPrivDBKey(domain)
	cmd := d.client.Set(ctx, key, keyBytes, 0)

	return cmd.Err()
}

// GetPrivateKey stores the certificate for a given domain in redis.
func (d *RedisDatastore) SaveCertificate(domain string, cert []byte) error {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.SaveCertificate: Saving certificate")
	key := getCertDBKey(domain)
	cmd := d.client.Set(ctx, key, cert, 0)

	return cmd.Err()
}
