// memcached_datastore is a package that provides a datastore.Datastore implementation,
// connecting to Memcached
package memcached_datastore

import (
	"fmt"

	"github.com/bradfitz/gomemcache/memcache"
	"gitlab.com/space55/keyless-tls-terminator/datastore"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// MemcachedDatastore is the datastore implementation for Memcached. It implements the datastore.Datastore interface,
// and can be provided to the NKS to store and retrieve private keys and certificates.
type MemcachedDatastore struct {
	memcachedAddresses []string
	memcachedNetwork   string
	client             *memcache.Client
	datastore.Datastore
}

// NewMemcachedDatastore creates a new MemcachedDatastore for a given set of parameters. It returns a *MemcachedDatastore,
// which can be used by the NKS to store and retrieve private keys and certificates.
func NewMemcachedDatastore(addr []string, network string) (*MemcachedDatastore, error) {
	kttlog.Logger.Debug().Strs("addrs", addr).Msg("memcached_datastore.NewMemcachedDatastore: Creating new datastore")
	d := &MemcachedDatastore{
		memcachedAddresses: addr,
		memcachedNetwork:   network,
	}

	mc := memcache.New(addr...)
	kttlog.Logger.Trace().Strs("addrs", addr).Msg("memcached_datastore.NewMemcachedDatastore: Datastore created")

	d.client = mc

	err := d.client.Ping()
	if err != nil {
		return nil, err
	}

	return d, nil
}

func getPrivDBKey(domain string) string {
	return fmt.Sprintf("%s:privkey", domain)
}

func getCertDBKey(domain string) string {
	return fmt.Sprintf("%s:cert", domain)
}

// Close terminates the connection to the Memcached server. Note that this function
// is a no-op, as the memcached library used does not support the Close function.
func (d *MemcachedDatastore) Close() error {
	return nil
}

// GetPrivateKey retrieves the private key for a given domain from memcached.
func (d *MemcachedDatastore) GetPrivateKey(domain string) ([]byte, error) {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.GetPrivateKey: Getting private key")
	key := getPrivDBKey(domain)
	item, err := d.client.Get(key)
	if err != nil {
		return nil, err
	}

	return item.Value, nil
}

// GetPrivateKey retrieves the certificate for a given domain from memcached.
func (d *MemcachedDatastore) GetCertificate(domain string) ([]byte, error) {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.GetCertificate: Getting certificate")
	key := getCertDBKey(domain)
	item, err := d.client.Get(key)
	if err != nil {
		return nil, err
	}

	return item.Value, nil
}

// GetPrivateKey stores the private key for a given domain in memcached.
func (d *MemcachedDatastore) SavePrivateKey(domain string, keyBytes []byte) error {
	kttlog.Logger.Trace().Str("domain", domain).Msg("RedisDatastore.SavePrivateKey: Saving private key")
	key := getPrivDBKey(domain)
	item := &memcache.Item{
		Key:   key,
		Value: keyBytes,
	}

	return d.client.Set(item)
}

// GetPrivateKey stores the certificate for a given domain in memcached.
func (d *MemcachedDatastore) SaveCertificate(domain string, certBytes []byte) error {
	kttlog.Logger.Trace().Str("domain", domain).Msg("MemcachedDatastore.SaveCertificate: Saving certificate")
	key := getCertDBKey(domain)
	item := &memcache.Item{
		Key:   key,
		Value: certBytes,
	}

	return d.client.Set(item)
}
