// This Datastore package holds a sole interface, which is used to interface with external
// storage mechanisms, such as Redis or Memcached, for storing private keys and their
// certificates.
package datastore

// Datastore is an interface that defines the methods that a datastore must implement. It allows
// for retrieval of certificates and private keys from a remote database of some sort.
// WARNING: The security of this entire system is only as secure as its weakest link - make sure
// that the datastore is secure.
type Datastore interface {
	// GetPrivateKey gets the private key associated with the given domain.
	GetPrivateKey(domain string) ([]byte, error)
	// GetCertificate gets the certificate associated with the given domain.
	GetCertificate(domain string) ([]byte, error)
	// SavePrivateKey saves the private key associated with the given domain.
	SavePrivateKey(domain string, key []byte) error
	// SaveCertificate saves the certificate associated with the given domain.
	SaveCertificate(domain string, cert []byte) error
	// Close closes the datastore.
	Close() error
}
