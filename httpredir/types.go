// httpredir is a simple HTTP redirector. This is useful if you plan to build an entire
// HTTP reverse proxy on top of the proxy.TLSProxy struct, so you don't have to do the
// legwork of creating a redirector yourself. It is not necessary to use this, though,
// and everything this package provides can be accomplished with a simple Nginx config.
package httpredir

import (
	"net"
	"net/http"

	"github.com/kataras/muxie"
)

// RedirectHandler is a function which can be used to handle an HTTP request. It should redirect
// the request to a different URL, and return the status code of the redirect.
type RedirectHandler func(http.ResponseWriter, *http.Request)

// HTTPRedirector is the main HTTP redirector struct. It implements the net.Listener
// interface, and has an internal *muxie.Mux, which is used to handle incoming requests.
type HTTPRedirector struct {
	laddr        string
	lnetwork     string
	internalLn   net.Listener
	Mux          *muxie.Mux
	RedirHandler RedirectHandler
	addr         net.Addr
	net.Listener
}

// TLSRedirector is a TLS redirector, which is a wrapper around an HTTPRedirector.
// It will automatically return a 301 redirect to HTTPS, unless a different status
// code is specified in the struct.
type TLSRedirector struct {
	redir      *HTTPRedirector
	network    string
	addr       string
	StatusCode int
}

// PatternNotPresentError is an error type, which is returned when a pattern for a request URL is not found.
type PatternNotPresentError struct {
	Pattern string
}

// Error returns the error message for the PatternNotPresentError.
func (e *PatternNotPresentError) Error() string {
	return "Pattern not present: " + e.Pattern
}
