package httpredir

import (
	"net"
	"net/http"

	"github.com/kataras/muxie"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// NewHTTPRedirector will create a new *HTTPRedirector with the given parameters. It uses Muxie for
// routing internally, and can be have its variables modified and accessed by anyone wishing to extend it.
func NewHTTPRedirector(lnetwork, laddr string) *HTTPRedirector {
	kttlog.Logger.Debug().Str("addr", laddr).Msg("httpredir.NewHTTPRedirector: Creating new HTTP redirector")
	r := &HTTPRedirector{
		lnetwork: lnetwork,
		laddr:    laddr,
		Mux:      muxie.NewMux(),
	}

	r.RedirHandler = r.defaultHandleRequest
	r.Mux.HandleFunc("/*", r.localRoute)

	return r
}

// ListenAndServe starts the HTTPRedirector, and returns any error encountered during its lifetime.
// It is implemented by calling the Listen() and Serve() methods, and returning any error encountered
// by either call.
func (r *HTTPRedirector) ListenAndServe() error {
	err := r.Listen()
	if err != nil {
		return err
	}
	return r.Serve()
}

// Listen starts the HTTPRedirector on the previously specified network and address. It will return any error encountered
// starting the listener.
func (r *HTTPRedirector) Listen() error {
	kttlog.Logger.Info().Str("addr", r.laddr).Msg("HTTPRedirector.Listen: Listening for HTTP requests")
	ln, err := net.Listen(r.lnetwork, r.laddr)
	if err != nil {
		return err
	}

	r.internalLn = ln

	return nil
}

// Serve starts the HTTPRedirector, and returns any error encountered during its lifetime.
func (r *HTTPRedirector) Serve() error {
	kttlog.Logger.Trace().Msg("HTTPRedirector.Serve: Serving HTTP redirector")
	return http.Serve(r, r.Mux)
}

// Close closes the internal net.Listener, and returns any error encountered.
func (r *HTTPRedirector) Close() error {
	kttlog.Logger.Trace().Msg("HTTPRedirector.Close: Closing HTTP redirector")
	return r.internalLn.Close()
}

// Addr retusn the net.Addr of the internal net.Listener. It returns nil if the internal net.Listener is nil.
func (r *HTTPRedirector) Addr() net.Addr {
	if r.internalLn != nil {
		return r.internalLn.Addr()
	}
	return nil
}

// Accept waits for a new net.Conn, and returns it, along with any error encountered.
func (r *HTTPRedirector) Accept() (net.Conn, error) {
	kttlog.Logger.Trace().Msg("HTTPRedirector.Accept: Waiting to accept new connection")
	conn, err := r.internalLn.Accept()
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Debug().Str("addr", conn.RemoteAddr().String()).Msg("HTTPRedirector.Accept: Accepted new connection")
	return conn, nil
}

// AddRoute adds a new route to the HTTPRedirector, where traffic is handled by the specified RedirectHandler.
// Note that this will override the default RedirectHandler, or a RedirectHandler stored in the struct.
// It is recommended to have either all traffic handled by the RedirectHandler specified in the struct, or all
// traffic handled by custom handlers. In addition, the handler specified in the struct can be used as the "default",
// with any other handlers specified directly by calling this function.
func (r *HTTPRedirector) AddRoute(pattern string, handler RedirectHandler) {
	kttlog.Logger.Debug().Str("pattern", pattern).Msg("HTTPRedirector.AddRoute: Adding route")
	r.Mux.HandleFunc(pattern, handler)
}

func (r *HTTPRedirector) localRoute(w http.ResponseWriter, req *http.Request) {
	kttlog.Logger.Trace().Str("path", req.URL.Path).Msg("HTTPRedirector.localRoute: Handling local route")
	r.RedirHandler(w, req)
}
