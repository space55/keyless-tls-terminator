package httpredir_test

import (
	"errors"
	"net"
	"net/http"
	"testing"

	"github.com/rs/zerolog"
	"gitlab.com/space55/keyless-tls-terminator/httpredir"
)

type tlsTester struct {
	t *testing.T
	r *httpredir.TLSRedirector
}

func TestTLSRedir(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	t.Log("Running TestTLSRedir")
	r := httpredir.NewTLSRedirector("tcp", "localhost:8080")

	t.Log("Redirector created")
	tester := &tlsTester{t: t, r: r}
	r.Listen()
	go tester.Run()

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	resp, err := client.Get("http://localhost:8080/abcde?abcde=abcde")
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusMovedPermanently {
		t.Fatal("Expected status code 301, got", resp.StatusCode)
	}

	t.Log("Got response: ", resp.StatusCode)

	err = resp.Body.Close()
	if err != nil {
		t.Fatal(err)
	}

	err = tester.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func (r *tlsTester) Run() error {
	err := r.r.Serve()
	if err != nil {
		if errors.Is(err, net.ErrClosed) {
			r.t.Log("Got (hopefully) expected error: ", err)
			return nil
		}
		r.t.Fatal(err)
	}
	return nil
}

func (r *tlsTester) Close() error {
	return r.r.Close()
}
