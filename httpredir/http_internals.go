package httpredir

import "net/http"

func (r *HTTPRedirector) defaultHandleRequest(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(200)
}
