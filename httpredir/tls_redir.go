package httpredir

import (
	"fmt"
	"net/http"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// NewTLSRedirector extends the HTTPRedirector to handle HTTP requests, and redirect them to HTTPS. It accepts
// the same parameters as NewHTTPRedirector, and returns a new *TLSRedirector.
func NewTLSRedirector(lnetwork, laddr string) *TLSRedirector {
	kttlog.Logger.Info().Str("addr", laddr).Msg("TLSRedirector.NewTLSRedirector: Creating new TLS redirector")
	httpRedir := NewHTTPRedirector(lnetwork, laddr)
	r := &TLSRedirector{
		redir:      httpRedir,
		StatusCode: http.StatusMovedPermanently,
		network:    lnetwork,
		addr:       laddr,
	}

	r.redir.RedirHandler = r.tlsHandler

	return r
}

// ListenAndServe starts the TLSRedirector, and returns any error encountered during its lifetime. It is
// implemented by calling the Listen and Serve functions, and returning any error encountered by either call.
func (r *TLSRedirector) ListenAndServe() error {
	err := r.Listen()
	if err != nil {
		return err
	}

	return r.Serve()
}

// Listen starts the internal HTTPRedirector on the previously specified network and address. It will return any error
// encuntered by starting the listener.
func (r *TLSRedirector) Listen() error {
	kttlog.Logger.Info().Str("addr", r.addr).Msg("TLSRedirector.Listen: Listening for HTTP requests")
	return r.redir.Listen()
}

// Serve serves requests for the TLSRedirector, using the internal HTTPRedirector. It will return any error
// encountered during its lifetime.
func (r *TLSRedirector) Serve() error {
	return r.redir.Serve()
}

// Close closes the TLSRedirector, by closing the internal HTTPredirector. It will return any error encountered
func (r *TLSRedirector) Close() error {
	return r.redir.Close()
}

func (r *TLSRedirector) tlsHandler(w http.ResponseWriter, req *http.Request) {
	kttlog.Logger.Trace().Str("host", req.Host).Msg("TLSRedirector.tlsHandler: Handling redirect")

	newURL := "https://" + req.Host + req.URL.RequestURI()

	html := `<html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"><title>%d Moved</title></head><body><h1>%d Moved</h1>The document has moved <a href="%s">here</a>.</body></body>`

	html = fmt.Sprintf(html, r.StatusCode, r.StatusCode, newURL)

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Location", newURL)
	w.Header().Set("Server", "ktt-tlsr")
	w.WriteHeader(r.StatusCode)
	_, err := w.Write([]byte(html))
	if err != nil {
		kttlog.Logger.Error().Err(err).Msg("TLSRedirector.tlsHandler: Error writing to response")
	}
}
