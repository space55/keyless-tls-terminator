package proxy

import (
	"io"
	"net"
	"os"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

// NewSimpleProxy creates a new *SimpleProxy from a given configuration file path. It will return the *SimpleProxy struct, and any error
// encountered during the parsing of the config, or the creation of the struct. It is implemented by reading the config file, calling
// NewSimpleProxyFromParams, then applying any additional values from the config file as necessary.
func NewSimpleProxy(configPath string) (*SimpleProxy, error) {
	kttlog.Logger.Debug().Str("path", configPath).Msg("proxy.NewSimpleProxy: Creating new SimpleProxy from config file")
	conf, err := readConfig(configPath)
	if err != nil {
		return nil, err
	}

	s := NewSimpleProxyFromParams(conf.UpstreamNetwork, conf.UpstreamAddr, conf.LocalNetwork, conf.LocalAddr, conf.ServerNetwork, conf.ServerAddr, conf.CertPath, conf.KeyPath, conf.CAPath)
	if err != nil {
		return nil, err
	}

	err = applyConfig(conf, s)
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Trace().Msg("proxy.NewSimpleProxy: SimpleProxy created")

	return s, nil
}

// NewSimpleProxyFromParams creates a new SimpleProxy struct from the given parameters. It will return the *SimpleProxy struct it creates
func NewSimpleProxyFromParams(upstreamNetwork, upstreamAddr, lnetwork, laddr, keyserverNetwork, keyserverAddr, certpath, keypath string, capath []string) *SimpleProxy {
	kttlog.Logger.Info().Msg("proxy.NewSimpleProxyFromParams: Creating new SimpleProxy")
	proxy := &SimpleProxy{
		upstreamAddr:    upstreamAddr,
		upstreamNetwork: upstreamNetwork,
		lnetwork:        lnetwork,
		laddr:           laddr,
		rnetwork:        keyserverNetwork,
		raddr:           keyserverAddr,
		certpath:        certpath,
		keypath:         keypath,
		capath:          capath,
	}

	return proxy
}

// Prepare prepares the SimpleProxy struct for use. It will return an error if it encounters any problems.
// It is implemented by creating the TLSProxy struct it uses internally, loads any remote certificates,
// and starts to listen on the previously specified address/interface.
func (sp *SimpleProxy) Prepare() error {
	sp.prepared = true
	proxy, err := NewTLSProxy(sp.rnetwork, sp.raddr, sp.certpath, sp.keypath, sp.capath, sp.trafficHandler)
	sp.proxy = proxy
	if err != nil {
		return err
	}

	if sp.upstreamNetwork == "file" {
		f, err := os.OpenFile(sp.upstreamAddr, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
		if err != nil {
			return err
		}
		sp.upstreamWriter = f
	}

	err = sp.proxy.LoadRemoteCerts(sp.remoteCertPaths)
	if err != nil {
		return err
	}

	if sp.shouldExportMasterKeys {
		sp.proxy.OutputMasterKeysToFile(sp.masterKeysFile)
	}

	if !sp.prepared {
		err := sp.Prepare()
		if err != nil {
			return err
		}
	}

	kttlog.Logger.Debug().Str("addr", sp.laddr).Msg("SimpleProxy.Prepare: Starting proxy server")
	err = sp.proxy.Listen(sp.lnetwork, sp.laddr)
	if err != nil {
		return err
	}

	return err
}

// ListenAndServe starts the proxy server. It will return an error if it encounters any problems.
// It is implemented by calling the Prepare function, if it has not already been called,
// then calls the internal TLSProxy struct's Serve function.
func (sp *SimpleProxy) ListenAndServe() error {
	if !sp.prepared {
		err := sp.Prepare()
		if err != nil {
			return err
		}
	}
	kttlog.Logger.Info().Str("addr", sp.laddr).Msg("SimpleProxy.ListenAndServe: Serving proxy server")
	return sp.proxy.Serve()
}

// Close closes the SimpleProxy server. It will return an error if it encounters any problems.
func (sp *SimpleProxy) Close() error {
	if sp.upstreamNetwork == "file" {
		_ = sp.upstreamWriter.Close()
	}
	return sp.proxy.Stop()
}

func (sp *SimpleProxy) pipe(dst io.Writer, src io.Reader, chanClosed chan error) {
	_, err := io.Copy(dst, src)
	chanClosed <- err
}

func (sp *SimpleProxy) trafficHandler(downstream net.Conn) error {
	kttlog.Logger.Info().Str("addr", downstream.RemoteAddr().String()).Msg("SimpleProxy.trafficHandler: Accepting new connection")

	if sp.upstreamNetwork == "file" {
		kttlog.Logger.Debug().Msg("SimpleProxy.trafficHandler: Upstream is a file")
		return Pipe(sp.upstreamWriter, downstream)
	}

	upstream, err := net.Dial(sp.upstreamNetwork, sp.upstreamAddr)
	if err != nil {
		return err
	}

	return PipeConns(upstream, downstream)
}

// OutputMasterKeysToFile outputs the master keys to a file. See the documentation on the TLSProxy struct for more information.
// WARNING: Do not use this in production. This is only for testing purposes. It will output all TLS master keys to disk, allowing
// anybody with access to that file to decrypt all current and historical sessions owned by the same process.
func (sp *SimpleProxy) OutputMasterKeysToFile(path string) {
	sp.masterKeysFile = path
	sp.shouldExportMasterKeys = true
	if sp.proxy != nil {
		sp.proxy.OutputMasterKeysToFile(path)
	}
}

// LoadRemoteCerts loads certificates into the TLSProxy struct. Private keys should _not_ be specified here. This is used
// to reduce round-trip-time for any connection, as it does not have to fetch certificates for any new connection.
func (sp *SimpleProxy) LoadRemoteCerts(paths []string) {
	sp.remoteCertPaths = paths
}
