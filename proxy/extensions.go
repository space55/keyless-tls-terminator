package proxy

import (
	"crypto/tls"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func (proxy *TLSProxy) defaultConfigRetriever(hello *tls.ClientHelloInfo) (*tls.Config, error) {
	kttlog.Logger.Debug().Str("server_name", hello.ServerName).Msg("Getting default config")

	c := &tls.Config{
		GetCertificate:     proxy.GetCertificate,
		GetConfigForClient: proxy.ConfigRetriever,
		ClientSessionCache: proxy.ClientSessionCache,
		MinVersion:         uint16(proxy.MinTLSVersion),
	}
	if proxy.shouldExportMasterKeys {
		c.KeyLogWriter = proxy.masterKeysWriter
	}

	return c, nil
}

func newDefaultClientSessionCache() *ProxyClientSessionCache {
	c := &ProxyClientSessionCache{
		sessions:     make(map[string]*tls.ClientSessionState),
		sessionsLock: &sync.RWMutex{},
	}

	return c
}

// Get returns the ClientSessionState associated with the given key.
func (c *ProxyClientSessionCache) Get(sessionKey string) (*tls.ClientSessionState, bool) {
	session, ok := c.sessions[sessionKey]
	return session, ok
}

// Put adds the ClientSessionState to the cache with the given key.
func (c *ProxyClientSessionCache) Put(sessionKey string, cs *tls.ClientSessionState) {
	c.sessionsLock.Lock()
	defer c.sessionsLock.Unlock()

	c.sessions[sessionKey] = cs
}
