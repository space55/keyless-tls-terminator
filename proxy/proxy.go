package proxy

import (
	"crypto/tls"
	"errors"
	"io"
	"net"
	"os"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/nokeyclient"
)

// NewTLSProxy creates a new *TLSProxy from the given parameters. It returns an error in the event that the CA files cannot be parsed,
// or the client cannot connect to the NKS
func NewTLSProxy(rnetwork, raddr, certpath, keypath string, capath []string, trafficHandler TrafficHandler) (*TLSProxy, error) {
	kttlog.Logger.Debug().Msg("proxy.NewTLSProxy: Creating new TLSProxy")
	nkc, err := nokeyclient.NewNoKeyClient(rnetwork, raddr, certpath, keypath, capath)
	if err != nil {
		return nil, err
	}

	caPool, err := netcommon.NewCertPool(capath)
	if err != nil {
		return nil, err
	}

	proxy := &TLSProxy{
		client:             nkc,
		caPool:             caPool,
		TrafficHandler:     trafficHandler,
		ClientSessionCache: newDefaultClientSessionCache(),
		MinTLSVersion:      tls.VersionTLS12,
	}

	proxy.ConfigRetriever = proxy.defaultConfigRetriever

	err = nkc.Connect()
	if err != nil {
		return nil, err
	}

	return proxy, nil
}

// ListenAndServe starts the TLSProxy on the given network and address, and serves traffic to the previously specified TrafficHandler.
// It is implemented by calling the proxy.Listen() and proxy.Serve() methods, and returns any error from either call
func (proxy *TLSProxy) ListenAndServe(network, addr string) error {
	err := proxy.Listen(network, addr)
	if err != nil {
		return err
	}

	return proxy.Serve()
}

// Listen starts the TLSProxy on the given network and address, and returns any error from the call to net.Listen
func (proxy *TLSProxy) Listen(network, addr string) error {
	ln, err := net.Listen(network, addr)
	if err != nil {
		return err
	}

	proxy.internalLn = ln
	proxy.listenNetwork = network

	return nil
}

// SetListener sets the internal listener to the one specified in the function call. This is useful for using a custom net.Listener,
// and then having that listener's TLS connections terminated by the proxy struct. Do not use this function if you are using the Listen function,
// as they conflict
func (proxy *TLSProxy) SetListener(listener net.Listener) {
	proxy.internalLn = listener
}

// Stop stops the TLSProxy, and closes the internal listener
func (proxy *TLSProxy) Stop() error {
	return proxy.internalLn.Close()
}

// Addr returns the net.Addr of the internal net.Listener
func (proxy *TLSProxy) Addr() net.Addr {
	return proxy.internalLn.Addr()
}

// Serve serves traffic to the TLSProxy. It is implemented by calling the proxy.Accept() function, and then calling the proxy.HandleConn()
// function on each new net.Conn
func (proxy *TLSProxy) Serve() error {
	kttlog.Logger.Info().Str("addr", proxy.internalLn.Addr().String()).Msg("TLSProxy.Serve: Starting TLSProxy")
	for {
		conn, err := proxy.Accept()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				kttlog.Logger.Info().Msg("TLSProxy.Serve: Proxy server stopped")
				return nil
			}
			kttlog.Logger.Err(err).Msg("TLSProxy.Serve: Error while accepting new connection")
			continue
		}
		go proxy.HandleConn(conn)
	}
}

// Accept accepts a new connection from the internal net.Listener, and returns the net.Conn and any error from the call to net.Accept
func (proxy *TLSProxy) Accept() (net.Conn, error) {
	conn, err := proxy.internalLn.Accept()
	if err != nil {
		return nil, err
	}

	return proxy.PrepareConn(conn)
}

// PrepareConn accepts a net.Conn, terminates TLS, and returns a net.Conn that automatically encrypts & handles any traffic.
// It returns any error from processing the TLS handshake
func (proxy *TLSProxy) PrepareConn(conn net.Conn) (*tls.Conn, error) {
	kttlog.Logger.Info().Str("addr", conn.RemoteAddr().String()).Msg("TLSProxy.PrepareConn: Preparing new connection")

	cfg := &tls.Config{
		GetCertificate:     proxy.GetCertificate,
		GetConfigForClient: proxy.ConfigRetriever,
		ClientSessionCache: proxy.ClientSessionCache,
		MinVersion:         uint16(proxy.MinTLSVersion),
	}
	if proxy.shouldExportMasterKeys {
		cfg.KeyLogWriter = proxy.masterKeysWriter
	}

	tlsConn := tls.Server(conn, cfg)

	err := tlsConn.Handshake()
	if err != nil {
		return nil, err
	}

	kttlog.Logger.Info().Msg("TLSProxy.Accept: Handshake successful")

	return tlsConn, nil
}

// HandleConn accepts a net.Conn, and sends the traffic on to the proxy.TrafficHandler. It returns any error from the call to proxy.TrafficHandler
func (proxy *TLSProxy) HandleConn(conn net.Conn) error {
	err := proxy.TrafficHandler(conn)
	if err != nil {
		kttlog.Logger.Err(err).Msg("TLSProxy.HandleConn: Error while handling connection")
	}
	return err
}

// OutputMasterKeysToFile saves all TLS master keys to the given file. It returns any error from the call to os.Create.
// WARNING: This function will dump all TLS master keys to the given file. This is a security risk, as anyone that gains access
// to that file can decrypt _all_ active connections, and any historical ones that have been terminated by the proxy.
func (proxy *TLSProxy) OutputMasterKeysToFile(path string) {
	kttlog.Logger.Warn().Msg("proxy.OutputMasterKeysToFile: Warning: This function exports all TLS master keys to disk, and should *ONLY* be used for debugging purposes")

	f, err := os.Create(path)
	if err != nil {
		kttlog.Logger.Fatal().Err(err).Msg("TLSProxy.OutputMasterKeysToFile: Error while creating master key file")
	}

	proxy.masterKeysWriter = &MasterKeyLogWriter{
		writeLock: &sync.Mutex{},
		path:      path,
		writer:    f,
	}

	proxy.shouldExportMasterKeys = true
	proxy.masterKeysDir = path
}

// VerifyReceivedSignatures enables receive-side signature verification. This is only useful as a debug step, to ensure
// that signatures received from the NKS are valid. This should not be enabled in production, as it will significantly
// increase the time it takes to terminate a connection.
func (proxy *TLSProxy) VerifyReceivedSignatures() {
	proxy.client.VerifyReceivedSignatures = true
}

// Pipe connects an io.Writer to an io.Reader, and returns any resulting error. This is implemented by creating a chan error,
// and then calling PipeWithErrorToChan, and returning any error received by the error chan, if any.
func Pipe(dst io.Writer, src io.Reader) error {
	errChan := make(chan error)
	go PipeWithErrorToChan(dst, src, errChan)
	return <-errChan
}

// PipeWithErrorToChan connects an io.Writer to an io.Reader, and will send any errors it encounters to the given error chan.
// This is useful for piping multiple io.Writer/io.Reader pairs with goroutines, and waiting for any error to occur in any
// of the goroutines.
func PipeWithErrorToChan(dst io.Writer, src io.Reader, errChan chan error) {
	_, err := io.Copy(dst, src)
	errChan <- err
}

// PipeConns connects two net.Conn structs, and returns any resulting error. This is implemented by creating a chan error, using
// the PipeWithErrorToChan function, and returning any error received by the error chan, if any. If any values are sent over
// the chan, including nil values, it closes the two net.Conn structs.
func PipeConns(conn1, conn2 net.Conn) error {
	kttlog.Logger.Debug().Str("addr1", conn1.RemoteAddr().String()).Str("addr2", conn2.RemoteAddr().String()).Msg("proxy.PipeConns: Piping connections")
	chanClosed := make(chan error)

	go PipeWithErrorToChan(conn1, conn2, chanClosed)
	go PipeWithErrorToChan(conn2, conn1, chanClosed)

	err := <-chanClosed
	if err != nil {
		_ = conn1.Close()
		_ = conn2.Close()
		return err
	}

	err = conn1.Close()
	if err != nil {
		_ = conn2.Close()
		return err
	}

	return conn2.Close()
}
