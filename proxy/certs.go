package proxy

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"

	"github.com/juliangruber/go-intersect"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
)

// LoadRemoteCerts loads certificates hosted by the NKS. This is used to determine if the proxy
// should be able to serve a certificate for a given domain. Private keys should _not_ be specified,
// as they are stored in the NKS. This accepts a list of paths to certificates, and will load them
// from disk on call.
func (proxy *TLSProxy) LoadRemoteCerts(certs []string) error {
	return proxy.client.LoadRemoteCerts(certs)
}

// GetPrivateKey returns a *netcommon.RemotePrivateKey for the given domain. See the documentation
// for netcommon.RemotePrivateKey for more information.
func (proxy *TLSProxy) GetPrivateKey(domain string) (*netcommon.RemotePrivateKey, error) {
	remoteCert := proxy.client.GetRemoteCert(domain)
	if remoteCert == nil {
		kttlog.Logger.Error().Str("domain", domain).Msg("TLSProxy.GetPrivateKey: No loaded remote certificate found")
		return nil, &NoRemoteCertErr{domain}
	}

	privKey := &netcommon.RemotePrivateKey{
		Domain:          domain,
		RemoteProofFunc: proxy.client.NewRequestProofFunc(domain),
		PublicKey:       remoteCert.Leaf.PublicKey,
		RemoteTLSCert:   remoteCert,
		RemoteCert:      remoteCert.Leaf,
	}

	return privKey, nil
}

// GetCertificate returns a *tls.Certificate for the *tls.ClientHelloInfo. It additionally returns any errors encountered.
// It is implemented by creating a netcommon.RemotePrivateKey, which is a struct that supports the crypto.Signer interface,
// and allows signing of a TLS certificate request by a remote server. See the documentation for netcommon.RemotePrivateKey
// for more information.
func (proxy *TLSProxy) GetCertificate(hello *tls.ClientHelloInfo) (*tls.Certificate, error) {
	kttlog.Logger.Debug().Str("domain", hello.ServerName).Msg("TLSProxy.GetCertificate: Remote private key requested")

	evt := kttlog.Logger.Trace()
	evt = evt.Uints16("supported_tls_versions", hello.SupportedVersions)
	evt = evt.Uints16("supported_ciphersuites", hello.CipherSuites)
	evt = evt.Interface("supported_signature_schemes", hello.SignatureSchemes)
	evt.Msg("TLSProxy.GetCertificate: Found supported TLS parameters")

	remotePrivateKey, err := proxy.GetPrivateKey(hello.ServerName)
	if err != nil {
		return nil, err
	}

	if remotePrivateKey.RemoteCert.PublicKeyAlgorithm == x509.RSA {
		supportedSigSchemes := intersect.Hash(hello.SignatureSchemes, rsaSignatureSchemes)
		kttlog.Logger.Trace().Interface("mutual_signature_schemes", supportedSigSchemes).Msg("TLSProxy.GetCertificate: Found mutually supported signature schemes")

		if len(supportedSigSchemes) == 0 {
			return nil, x509.ErrUnsupportedAlgorithm
		}

		isTLS1_3 := false
		for _, vers := range hello.SupportedVersions {
			if vers == tls.VersionTLS13 {
				isTLS1_3 = true
				break
			}
		}

		if isTLS1_3 {
			kttlog.Logger.Trace().Msg("TLSProxy.GetCertificate: Version is TLS 1.3")
			supportedSigSchemes = intersect.Hash(supportedSigSchemes, pssSignatureSchemes)
			if len(supportedSigSchemes) == 0 {
				return nil, x509.ErrUnsupportedAlgorithm
			}
		}

		switch supportedSigSchemes[0] {
		case tls.PKCS1WithSHA256:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PKCS1
		case tls.PKCS1WithSHA384:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PKCS1
		case tls.PKCS1WithSHA512:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PKCS1
		case tls.PSSWithSHA256:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PSS
		case tls.PSSWithSHA384:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PSS
		case tls.PSSWithSHA512:
			remotePrivateKey.SigType = netcommon.SigType_RSA_PSS
		default:
			remotePrivateKey.SigType = netcommon.SigType_DEFAULT
		}

		kttlog.Logger.Debug().Stringer("selected_signature_scheme", supportedSigSchemes[0].(fmt.Stringer)).Stringer("sig_type", remotePrivateKey.SigType).Msg("TLSProxy.GetCertificate: Selected signature scheme")
		if remotePrivateKey.SigType == netcommon.SigType_DEFAULT {
			kttlog.Logger.Error().Msg("TLSProxy.GetCertificate: No supported signature scheme found")
			return nil, x509.ErrUnsupportedAlgorithm
		}
	}

	cert := &tls.Certificate{
		PrivateKey:                   remotePrivateKey,
		Leaf:                         remotePrivateKey.RemoteCert,
		Certificate:                  remotePrivateKey.RemoteTLSCert.Certificate,
		SupportedSignatureAlgorithms: defaultSigSchemes,
	}

	return cert, hello.SupportsCertificate(cert)
}
