package proxy

// Write sends any data to the previously specified TLS master keys file
func (w *MasterKeyLogWriter) Write(p []byte) (n int, err error) {
	w.writeLock.Lock()
	defer w.writeLock.Unlock()

	return w.writer.Write(p)
}
