// proxy is the main package that handles all TLS termination. It offers the TLSProxy struct, which can be used to create your own
// TLS terminating reverse proxy. Optionally, you can use the SimpleProxy struct, which is a simple wrapper around the TLSProxy,
// with a default TrafficHandler that simply forwards traffic to the remote server.
package proxy

import (
	"crypto/tls"
	"crypto/x509"
	"io"
	"net"
	"sync"

	"gitlab.com/space55/keyless-tls-terminator/nokeyclient"
)

var defaultSigSchemes = []tls.SignatureScheme{
	tls.Ed25519,
	tls.ECDSAWithP521AndSHA512,
	tls.ECDSAWithP384AndSHA384,
	tls.ECDSAWithP256AndSHA256,
	tls.PSSWithSHA512,
	tls.PSSWithSHA384,
	tls.PSSWithSHA256,
	tls.PKCS1WithSHA512,
	tls.PKCS1WithSHA384,
	tls.PKCS1WithSHA256,
}

var rsaSignatureSchemes = []tls.SignatureScheme{
	tls.PSSWithSHA512,
	tls.PSSWithSHA384,
	tls.PSSWithSHA256,
	tls.PKCS1WithSHA512,
	tls.PKCS1WithSHA384,
	tls.PKCS1WithSHA256,
}

var pssSignatureSchemes = []tls.SignatureScheme{
	tls.PSSWithSHA512,
	tls.PSSWithSHA384,
	tls.PSSWithSHA256,
}

// NoRemoteCertErr is an error returned if the proxy does not know about a certificate for a ServerName it has been asked for
type NoRemoteCertErr struct {
	domain string
}

// TrafficHandler is a function which accepts a net.Conn, returns an error, and handles any traffic on the connection
type TrafficHandler func(net.Conn) error

// ConfigRetriever is a function which accepts a *tls.ClientHelloInfo, returns a *tls.Config, and any error occurred. This is
// used to terminate TLS connections with a custom configuration, including providing a specific crypto.Signer to use for
// proofs.
type ConfigRetriever func(*tls.ClientHelloInfo) (*tls.Config, error)

// TLSProxy is a TLS proxy which can be used to terminate TLS connections to a remote server, and implements net.Listener.
type TLSProxy struct {
	client        *nokeyclient.NoKeyClient
	listenNetwork string
	listen        net.Addr
	caPool        *x509.CertPool
	// TrafficHandler is the function which handles traffic on the connection. This value can be safely updated, but will only be used for future connections.
	TrafficHandler         TrafficHandler
	internalLn             net.Listener
	shouldExportMasterKeys bool
	masterKeysDir          string
	masterKeysWriter       *MasterKeyLogWriter
	// ConfigRetriever is the function which returns a *tls.Config for a given *tls.ClientHelloInfo
	ConfigRetriever ConfigRetriever
	// ClientSessionCache is the TLS session cache to use for the TLS proxy. This value can be safely updated, but will only be used for future connections.
	ClientSessionCache tls.ClientSessionCache
	// MinTLSVersion is the minimum TLS version the proxy supports. This value can be safely updated, but will only be used for future connections.
	// The values it supports are tls.VersionTLS10, tls.VersionTLS11, tls.VersionTLS12, tls.VersionTLS13.
	MinTLSVersion int
	net.Listener
}

// ProxyClientSessionCache is a sample TLS session cache which is used by the TLSProxy. It is a simple wrapper around a map of
// strings, associating with a *tls.ClientSessionState. It is thread safe, and implements the tls.ClientSessionCache interface
type ProxyClientSessionCache struct {
	sessions     map[string]*tls.ClientSessionState
	sessionsLock *sync.RWMutex
	tls.ClientSessionCache
}

// MasterKeyLogWriter is a struct which contains the necessary information to write master keys to a file
type MasterKeyLogWriter struct {
	path      string
	writeLock *sync.Mutex
	writer    io.WriteCloser
}

// SimpleProxyConfig is the configuration struct that all toml files are interpreted into. It is used to create a *SimpleProxy struct.
type SimpleProxyConfig struct {
	LocalAddr                string   `toml:"bind_addr"`
	LocalNetwork             string   `toml:"bind_network"`
	UpstreamAddr             string   `toml:"upstream_addr"`
	UpstreamNetwork          string   `toml:"upstream_network"`
	CertPath                 string   `toml:"client_cert_path"`
	KeyPath                  string   `toml:"client_key_path"`
	CAPath                   []string `toml:"ca_paths"`
	ServerAddr               string   `toml:"server_addr"`
	ServerNetwork            string   `toml:"server_network"`
	VerifyReceivedSignatures bool     `toml:"verify_received_signatures"`
	OutputTLSMasterKeys      bool     `toml:"output_tls_master_keys"`
	TLSMasterKeysDir         string   `toml:"tls_master_keys_dir"`
	RemoteCerts              []string `toml:"remote_certs"`
	LogFile                  string   `toml:"log_file"`
	EnableSyslog             bool     `toml:"enable_syslog"`
	SyslogNetwork            string   `toml:"syslog_network"`
	SyslogAddr               string   `toml:"syslog_addr"`
	MinTLSVersion            string   `toml:"min_tls_version"`
}

// Get the error text for a NoRemoteCertErr
func (e *NoRemoteCertErr) Error() string {
	return "no remote cert for " + e.domain
}

// SimpleProxy is a simple TLS proxy which can be used to terminate TLS connections to a remote server. It is not recommended to use this
// in production, however, this is only because most reverse proxies should be implemented on their own using a proxy.TLSProxy struct.
// This struct, however, _can_ be used in production if you are looking for a simple quick-and-dirty solution.
type SimpleProxy struct {
	upstreamNetwork        string
	upstreamAddr           string
	lnetwork               string
	laddr                  string
	rnetwork               string
	raddr                  string
	certpath               string
	keypath                string
	capath                 []string
	proxy                  *TLSProxy
	masterKeysFile         string
	shouldExportMasterKeys bool
	remoteCertPaths        []string
	upstreamWriter         io.WriteCloser
	prepared               bool
}

// InvalidTLSVersionError is an error that is returned in the event that an invalid TLS version
// was specified in the config file.
type InvalidTLSVersionError struct {
	specified_version string
}

// Error returns the error text for the InvalidTLSVersionError
func (e *InvalidTLSVersionError) Error() string {
	return "invalid TLS version specified: " + e.specified_version
}
