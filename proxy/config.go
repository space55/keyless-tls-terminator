package proxy

import (
	"crypto/tls"

	"github.com/BurntSushi/toml"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
)

func applyConfig(conf *SimpleProxyConfig, s *SimpleProxy) error {
	kttlog.Logger.Debug().Msg("proxy.applyConfig: Applying config")

	s.LoadRemoteCerts(conf.RemoteCerts)

	if conf.VerifyReceivedSignatures {
		s.proxy.client.VerifyReceivedSignatures = true
	}

	if conf.OutputTLSMasterKeys {
		s.proxy.OutputMasterKeysToFile(conf.TLSMasterKeysDir)
	}

	if conf.LogFile != "" {
		err := kttlog.EnableFileLog(conf.LogFile)
		if err != nil {
			return err
		}
	}

	if conf.EnableSyslog {
		if conf.SyslogNetwork == "" {
			err := kttlog.EnableLocalSyslog()
			if err != nil {
				return err
			}
		} else {
			err := kttlog.EnableNetworkSyslog(conf.SyslogNetwork, conf.SyslogAddr)
			if err != nil {
				return err
			}
		}
	}

	if conf.MinTLSVersion != "" {
		switch conf.MinTLSVersion {
		case "1.3":
			s.proxy.MinTLSVersion = tls.VersionTLS13
		case "1.2":
			s.proxy.MinTLSVersion = tls.VersionTLS12
		case "1.1":
			s.proxy.MinTLSVersion = tls.VersionTLS11
		case "1.0":
			s.proxy.MinTLSVersion = tls.VersionTLS10
		default:
			return &InvalidTLSVersionError{
				specified_version: conf.MinTLSVersion,
			}
		}
	}

	kttlog.Logger.Trace().Msg("proxy.applyConfig: Finished applying config")
	return nil
}

func getDefaults() *SimpleProxyConfig {
	conf := &SimpleProxyConfig{
		LocalNetwork:    "tcp",
		UpstreamNetwork: "tcp",
		ServerNetwork:   "tcp",
	}

	return conf
}

func readConfig(path string) (*SimpleProxyConfig, error) {
	conf := getDefaults()

	_, err := toml.DecodeFile(path, conf)

	kttlog.Logger.Trace().Msg("nokeyserver.readConfig: Decoded config")

	return conf, err
}
