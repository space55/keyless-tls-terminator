package nokeyclient

import (
	"reflect"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

func (c *NoKeyClient) sendAndWait(op *proto.Op) (*proto.Op, error) {
	kttlog.Logger.Trace().Uint32("op_id", uint32(op.GetID())).Msg("NoKeyClient.sendAndWait: Sending op")
	opID := op.GetID()

	c.replyChans[opID] = make(chan *proto.Op)

	err := c.send(op)
	if err != nil {
		return nil, err
	}

	rep := <-c.replyChans[opID]
	delete(c.replyChans, opID)

	kttlog.Logger.Trace().Uint32("op_id", uint32(op.GetID())).Int("len", len(rep.GetContent())).Msg("NoKeyClient.sendAndWait: Received reply")

	return rep, nil
}

func (c *NoKeyClient) send(op *proto.Op) error {
	return c.client.WriteOp(op)
}

func (c *NoKeyClient) handleReply(op *proto.Op, replyingToID proto.OpID) {
	kttlog.Logger.Info().Uint32("replying_op_id", uint32(op.GetID())).Uint32("op_id", uint32(replyingToID)).Msg("NoKeyClient.handleReply: Handling reply")
	if c, ok := c.replyChans[replyingToID]; ok {
		c <- op
	} else {
		kttlog.Logger.Error().Uint32("replying_op_id", uint32(op.GetID())).Uint32("op_id", uint32(replyingToID)).Msg("NoKeyClient.handleReply: No reply channel for op")
	}
}

func (c *NoKeyClient) messageHandler(msg *proto.Message) {
	ops := msg.GetOps()
	for i := range ops {
		op := &ops[i]
		opData, err := op.Unmarshal()
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("NoKeyClient.messageHandler: Error unmarshaling op")
			continue
		}
		kttlog.Logger.Trace().Str("type", reflect.TypeOf(opData).String()).Msg("NoKeyClient.messageHandler: Handling op")
		if replyingOp, ok := opData.(proto.ReplyingOp); ok {
			kttlog.Logger.Trace().Uint32("op_id", uint32(replyingOp.GetReplyingTo())).Msg("NoKeyClient.messageHandler: Handling reply")
			go c.handleReply(op, replyingOp.GetReplyingTo())
		}
	}
}
