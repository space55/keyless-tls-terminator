// nokeyclient is the implementation of a client that connects to an NKS.
package nokeyclient

import (
	"crypto/tls"
	"crypto/x509"

	"gitlab.com/space55/keyless-tls-terminator/client"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

// NoKeyClient is the client struct which handles the connections to the remote NKS.
type NoKeyClient struct {
	// VerifyReceivedSignatures indicates whether or not the client should verify any signatures received from the remote NKS, before
	// returning them. This is useful for debugging, but should be disabled in production.
	VerifyReceivedSignatures bool
	remoteAddr               string
	remoteNetwork            string
	cert                     *tls.Certificate
	client                   *client.Client
	rootCAs                  *x509.CertPool
	replyChans               map[proto.OpID]chan *proto.Op
	remoteCerts              map[string]*tls.Certificate
}
