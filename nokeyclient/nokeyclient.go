package nokeyclient

import (
	"crypto/tls"
	"crypto/x509"

	"gitlab.com/space55/keyless-tls-terminator/client"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

// NewNoKeyClient creates a new NoKeyClient with the specified parameters. rnetwork and raddr are the network and address of the remote NKS server.
// certpath and keypath are the paths to the TLS certificate and private key for the client (not the ones that will be used to sign TLS requests
// by the proxy). capath is the path to the CA certificate bundle that signed the certificate and key present at certpath. It will return a new
// NoKeyClient, and any error encountered during loading the key pair, creating the x509.CertPool, or creating the internal client.Client struct.
func NewNoKeyClient(rnetwork, raddr, certpath, keypath string, capath []string) (*NoKeyClient, error) {
	kttlog.Logger.Debug().Msg("nokeyclient.NewNoKeyClient: Creating new NoKeyClient")
	cert, err := netcommon.LoadKeyPair(certpath, keypath)
	if err != nil {
		return nil, err
	}

	rootCAs := x509.NewCertPool()
	for _, path := range capath {
		err = netcommon.ReadCAPath(path, rootCAs)
		if err != nil {
			kttlog.Logger.Error().Err(err).Msg("nokeyclient.NewNoKeyClient: Failed to read CA path")
		}
	}

	cli := &NoKeyClient{
		remoteAddr:    raddr,
		remoteNetwork: rnetwork,
		cert:          cert,
		rootCAs:       rootCAs,
		replyChans:    make(map[proto.OpID]chan *proto.Op),
		remoteCerts:   make(map[string]*tls.Certificate),
	}

	cli.client = client.NewClient(raddr, cert, rootCAs, cli.messageHandler)

	return cli, nil
}

// Connect connects the NoKeyClient to the remote NKS. It returns any error encountered during the connection attempt.
func (c *NoKeyClient) Connect() error {
	err := c.client.Connect()
	if err != nil {
		_ = c.client.Disconnect()
		return err
	}

	kttlog.Logger.Info().Msg("nokeyclient.Connect: NoKeyClient connected")

	return nil
}

// Disconnect disconnects the NoKeyClient from the remote server.
func (c *NoKeyClient) Disconnect() error {
	return c.client.Disconnect()
}

// LoadRemoteCerts loads certificates for which the NKS has a corresponding private key.
// Private keys should _not_ be specified in this call. It will return any error encountered
// while processing.
func (c *NoKeyClient) LoadRemoteCerts(certs []string) error {
	for _, certPath := range certs {
		cert, err := netcommon.LoadTLSCertFromPath(certPath)
		if err != nil {
			return err
		}

		for _, name := range cert.Leaf.DNSNames {
			c.remoteCerts[name] = cert
		}
	}

	return nil
}

// GetRemoteCert returns a *tls.Certificate for the given hostname.
// Note that this TLS certificate will not contain a private key, and as such
// can not be used for signature operations.
func (c *NoKeyClient) GetRemoteCert(name string) *tls.Certificate {
	return c.remoteCerts[name]
}
