package nokeyclient

import (
	"crypto"

	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/netcommon"
	"gitlab.com/space55/keyless-tls-terminator/proto"
)

// NewRequestProofFunc generates a new RemoteProofFunc for a given domain. This is necessary
// because the default RemoteProofFunc requires a domain, but it is not provided elsewhere.
// This is primarily a wrapper/helper function.
func (c *NoKeyClient) NewRequestProofFunc(domain string) netcommon.RemoteProofFunc {
	return func(hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte) ([]byte, error) {
		return c.RequestProof(domain, hashFunc, sigType, challenge)
	}
}

// RequestProof requests a proof from the NKS server. It accepts the domain, hash function used by the proxy, the signature
// type used by the proxy, and the challenge sent by the proxy. It returns the proof, and any error encountered.
// Note that the challenge must *already* be hashed by the proxy. This is because the network layer is designed to be as
// low-bandwidth as possible, to fit a message into a single TCP packet.
func (c *NoKeyClient) RequestProof(domain string, hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte) ([]byte, error) {
	evt := kttlog.Logger.Debug()
	evt = evt.Str("domain", domain)
	evt = evt.Str("hash_func", hashFunc.String())
	evt = evt.Str("sig_type", sigType.String())
	evt.Msg("NoKeyClient.RequestProof: Requesting proof")

	op := &proto.Op_ProofReq{
		Domain:    domain,
		Challenge: challenge,
		HashFunc:  hashFunc,
		SigType:   sigType,
	}

	out, err := op.Marshal()
	if err != nil {
		return nil, err
	}

	b, err := proto.NewOp(proto.OpType_PROOFREQ, out)
	if err != nil {
		return nil, err
	}
	proof, err := c.sendAndWait(b)
	if err != nil {
		return nil, err
	}

	resp, err := proto.Unmarshal_ProofRep(proof.GetContent())
	if err != nil {
		return nil, err
	}

	evt = kttlog.Logger.Trace()
	evt = evt.Str("domain", domain)
	evt = evt.Uint32("op_id", uint32(resp.ReplyingToID))
	evt = evt.Int("len", len(resp.Proof))
	evt.Msg("NoKeyClient.RequestProof: Successfully received proof")

	if c.VerifyReceivedSignatures {
		kttlog.Logger.Debug().Str("domain", domain).Msg("NoKeyClient.RequestProof: Verifying received signature")
		err = c.VerifySignature(domain, hashFunc, sigType, challenge, resp.Proof)
		if err != nil {
			return nil, err
		}
		kttlog.Logger.Trace().Str("domain", domain).Msg("NoKeyClient.RequestProof: Signature verified")
	}

	return resp.Proof, nil
}

// VerifySignature verifies that a signature provided is valid for the given challenge. It will return any error
// encountered verifying the signature. A nil error indicates that the signature is valid.
func (c *NoKeyClient) VerifySignature(domain string, hashFunc crypto.Hash, sigType netcommon.SigType, challenge []byte, proof []byte) error {
	cert := c.remoteCerts[domain].Leaf
	return netcommon.Verify(cert.PublicKey, cert.PublicKeyAlgorithm, hashFunc, sigType, challenge, proof)
}
